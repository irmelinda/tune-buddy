import { APIKEY_BING_MAPS, BING_MAPS_URL } from "../common/constants";

const autoSuggest = (value, setSuggestions) => {
    fetch(`${BING_MAPS_URL}/Autosuggest?query=${value}&key=${APIKEY_BING_MAPS}`)
        .then(response => response.json())
        .then(result => {
            if (result.statusCode >= 400) {
                throw new Error(result.statusDescription)
            } else {
                setSuggestions(result.resourceSets[0].resources[0].value);
            }
        })
        .catch((error) => console.log(error));
}

export default autoSuggest;

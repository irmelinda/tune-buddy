import constants from "../common/constants";

const generateTracks = (valueSlider, tripLength, repeatArtists, setMessage, setError, setTracksValue, tracksValue) => {

  //setLoading(true);
  const token = localStorage.token;

  const newPercent = (value, sum) => {
    return Math.round(100 / sum * value);
  }

  const sum = valueSlider.reduce((acc, el) => acc + el.percent, 0)
  const formattedValueSlider = valueSlider
    .filter(el => el.percent !== 0)
    .map(el => ({ ...el, percent: newPercent(el.percent, sum) }));

  fetch(`${constants.BASE_URL}/playlists/generator`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${token}`
    },
    body: JSON.stringify({
      duration: tripLength,
      repeatArtist: repeatArtists,
      genres: formattedValueSlider,
    })
  })
    .then(response => response.json())
    .then(({ data, message }) => {

      if (!data) {
        setMessage(message);
      }
      setTracksValue(data);

    })
    .catch(error => setError(error))

};

export default generateTracks;

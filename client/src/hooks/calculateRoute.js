import { APIKEY_BING_MAPS, BING_MAPS_URL } from "../common/constants";


const calculateRoute = async (valueStart, valueEnd, setTripLength, setIfError, setMessage) => {
  //http://dev.virtualearth.net/REST/v1/Routes/Driving?wp.1=Sofia&wp.2=Varna&key=AuxK6BbN2mwTC8QOMUGArI661OQhEEzEish-MypKlQh0pwx2WhRKToWLYjGMeYaE
  fetch(`${BING_MAPS_URL}/Routes/Driving?wp.1=${valueStart}&wp.2=${valueEnd}&key=${APIKEY_BING_MAPS}`)
    .then(response => response.json())
    .then(result => {
      if (result.statusCode >= 400) {
        throw new Error(result.statusDescription)
      } else {
        setTripLength(result.resourceSets[0].resources[0].routeLegs[0].travelDuration)
        if(result.resourceSets[0].resources[0].routeLegs[0].travelDuration === 0) {
          setMessage('Sorry, we could not calculate travel duration')
          setIfError(true)
        } else {
          setMessage('')
          setIfError(false)
        }
      }

    })
    .catch((error) => setMessage('Sorry, we could not calculate travel duration'));
}

export default calculateRoute;

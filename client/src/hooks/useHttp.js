import { useEffect, useState } from 'react';

const useHttp = (url, initialData = null) => {
  const [data, setData] = useState(initialData);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null)
  const [message, setMessage] = useState(null);

  useEffect(() => {
    setLoading(true);
    const token = localStorage.token;
    let mounted = true;

    fetch(url, {
      headers: {
        authorization: `Bearer ${token}`
      },
    })
      .then(response => response.json())
      .then(({ data, message }) => {

        if (mounted) {
          if (!data) {
            setMessage(message);
          }
          setData(data);
        }
      })
      .catch(error => setError('error'))
      .finally(() => setLoading(false))

    const cleanup = () => {
      mounted = false;
    }

    return cleanup;
  }, [url]);

  return { data, loading, message, error };
};
export default useHttp;


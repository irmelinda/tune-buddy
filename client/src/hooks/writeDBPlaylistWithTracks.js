import constants from "../common/constants";

const writeDBPlaylistWithTracks = (playlistName, cover, setError, setNewPlaylistId, setMessage, props) => {


    const token = localStorage.token;

    fetch(`${constants.BASE_URL}/playlists`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            name: playlistName.value,
            cover_url: cover,
        })
    })
        .then(response => response.json())
        .then(({ data, message }) => {

            if (!data) {
                setMessage(message);
            }
            setNewPlaylistId(data[0].id);
           

        })
        .catch(error => setError(error))

};

export default writeDBPlaylistWithTracks;

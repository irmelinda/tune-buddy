const httpUnsplash = (url, setData, setError, setLoading, setMessage) => {

  fetch(url)
    .then(response => response.json())
    .then(({ urls }) => {
      if (!urls) {
        setMessage('There was a problem with Unsplash API.');
      }
      setData(urls.full);
    })
    .catch(error => setError(error))
    .finally(() => setLoading(false))
};
export default httpUnsplash;


const constants = {
  nameMinLength: 2,
  nameMaxLength: 45,
  userPasswordMinLength: 4,
  userPasswordMaxLength: 45,
  // emailRegex : /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-_]+)*$/,
  emailRegex : /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9-]+)*$/,
  passwordRegex: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 
  passwordMaxLength :255,
  usersCountMin: 5,
  usersCountMax: 10,

  playlistsCountMinValue:10,
  playlistsCountMaxValue:50,
  playlistsNameMinLength: 2,
  playlistsNameMaxLength: 255,

  BASE_URL: 'http://localhost:5555'
};

export default constants;

export const BING_MAPS_URL = 'http://dev.virtualearth.net/REST/v1';
//http://dev.virtualearth.net/REST/v1/Routes/Driving?wp.1=Sofia&wp.2=Varna&key=AuxK6BbN2mwTC8QOMUGArI661OQhEEzEish-MypKlQh0pwx2WhRKToWLYjGMeYaE

export const APIKEY_BING_MAPS = 'AuxK6BbN2mwTC8QOMUGArI661OQhEEzEish-MypKlQh0pwx2WhRKToWLYjGMeYaE';

export const date = new Date().getDate();
export const month = new Date().getMonth() + 1
export const year = new Date().getFullYear();
export const hour = new Date().getHours();
export const min = new Date().getMinutes();
export const sec = new Date().getSeconds()


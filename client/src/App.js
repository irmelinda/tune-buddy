import './App.css';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Layout from './components/Layout/Layout/Layout';
import Home from './pages/Home/Home';
import NotFound from './pages/NotFound/NotFound';
import Login from './pages/LogIn/Login';
import AllPlaylists from './pages/Playlists/MyPlaylists';
import Generator from './pages/Generator/Generator';
import Admin from './pages/Admin/Admin';
import Register from './pages/Register/Register';
import AuthContext, { getUser } from './providers/authContext';
import { useMemo, useState } from 'react';
import PrivateRoute from './extensions/PrivateRoute';
import TracksContext from './providers/tracksContext';
import HomeLogged from './pages/Home/HomeLoggedPages';
import SinglePlaylist from './pages/Playlists/SinglePlaylist';
import MyAccount from './pages/MyAccount/MyAccount';


const App = () => {
  const [authValue, setAuthState] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });
  console.log(authValue);

  const [tracksValue, setTracksValue] = useState([]);
  const providerTracksValue = useMemo(() => ({ tracksValue, setTracksValue }), [tracksValue, setTracksValue])

  return (
    <BrowserRouter className="app" >
      <AuthContext.Provider value={{ ...authValue, setAuthState }}>
        <TracksContext.Provider value={providerTracksValue}>
            <Layout>
              <Switch>
                
                <Redirect path="/" exact to="/home" />
                {authValue.isLoggedIn
                  ? <PrivateRoute path="/home" isLoggedIn={authValue.isLoggedIn} component={HomeLogged} />
                  : <Route path="/home" component={Home} />}

                <Route path="/login" component={Login} />
                <Route path="/create-account" component={Register} />

                <Route path="/playlists/:id" isLoggedIn={authValue.isLoggedIn} component={SinglePlaylist} />
                <PrivateRoute path="/generator" isLoggedIn={authValue.isLoggedIn} component={Generator} />
                <PrivateRoute path="/my-account/my-playlists" exact isLoggedIn={authValue.isLoggedIn} component={AllPlaylists} />
                <PrivateRoute path="/my-account/profile" isLoggedIn={authValue.isLoggedIn} component={MyAccount} />
                <PrivateRoute path="/admin" isLoggedIn={authValue.isLoggedIn} component={Admin} />
                <Route path="*" component={NotFound} />
              </Switch>
            </Layout>
        </TracksContext.Provider>
      </AuthContext.Provider >
    </BrowserRouter >
  );
}
export default App;

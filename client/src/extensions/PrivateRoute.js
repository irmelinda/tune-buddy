import { Route } from 'react-router-dom';
import React from 'react';

const PrivateRoute = ({ component: Component, isLoggedIn, ...rest }) => {

  return (
    <Route {...rest} render={(props) => isLoggedIn
      ? <Component {...props} />
      : 
      <>
      <p>You need to be logged in</p>
      <p>go to MY ACCOUNT to log in or register</p>
      <p>cancel only HOME is enough</p>
      </>
      } 
      />
  )
};

export default PrivateRoute;

import './FileUploader.css';
const ext = ['.jpg', '.jpeg', '.bmp', '.gif', '.png', '.svg'];
const isImage = (fileName) => {
    return ext.some(el => fileName.toLowerCase().endsWith(el));
}

const FileUploader = ({ onFileSelectError, onFileSelectSuccess, setShowRandom }) => {

    const handleFileInput = (e) => {
        const file = e.target.files[0];
        if (file && file.size > 1111111024) {
            onFileSelectError({ error: 'File size cannot exceed more than 1MB' });
        } else if (!isImage(file.name)) {
            onFileSelectError({ error: 'File type must be of image tab' });
        }
        else {
            setShowRandom(false)
            onFileSelectSuccess(file)
        };
    };

    return (
        <div className="file-uploader">
            <input type="file" accept="image/jpeg, image/jpg" onChange={handleFileInput} className="file-uploader"/>
        </div>
    )
}

export default FileUploader;

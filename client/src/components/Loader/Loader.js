import Loader from "react-loader-spinner";

const LoaderCustom = () => {

    return <Loader
        type="Audio"
        color="orange"
        height={100}
        width={100}
       
    />
};

export default LoaderCustom;

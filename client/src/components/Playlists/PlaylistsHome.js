import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PlaylistHome from '../Playlist/PlaylistHome';
import { withRouter } from 'react-router';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        justifyContent: 'left',
        border: 'none',
    },
    paper: {
        textAlign: 'left',
        backgroundColor: 'transparent',
        boxShadow: 'none'
    },
    grid: {
        justifyContent: 'space-between',
    },
}));

const Playlists = ({data, props, setData, showDeleteEdit}) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>

            <Grid container className={classes.grid}>

                {data && data.map(p => {
                    return (
                        <Paper className={classes.paper}>
                            <PlaylistHome
                                {...p}
                                goToOurPicksSingle={() => props.history.push(`/playlists/${p.id}`)}
                                key={p.id}
                                setData={setData}
                                data={data}
                                showDeleteEdit={showDeleteEdit}
                            />
                        </Paper>
                    )
                })
                }

            </Grid>
        </div>
    );
}

export default withRouter(Playlists);

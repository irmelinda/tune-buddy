import { Typography } from '@material-ui/core';
import { withRouter } from 'react-router';
import constants from '../../common/constants';
import Playlists from '../../components/Playlists/Playlists';
import useHttp from '../../hooks/useHttp';

const PlaylistsByDuration = ({ props, tripDuration }) => {

    const { data } = useHttp(
        `${constants.BASE_URL}/playlists?playlistsCount=4&duration=${tripDuration}`, []
    );

    return (
        <>
            {data && data.length>0
                ? <>
                    <Typography component="div" variant="h6">
                        YOU'VE GOT A MATCH
                </Typography>
                    <Typography component="div" variant="body1">
                        Soundtrack your ride with a suggested playlist
                </Typography>
                    <Playlists props={props} data={data} />
                    <Typography component="div" variant="body1">
                        or click next to customize your own
                  </Typography>
                </>
                : <>
                    <Typography component="div" variant="h6">
                        NO PLAYLIST MATCH YOUR TRAVEL DURATION
                </Typography>
                    <Typography component="div" variant="body1">
                        click next to customize your own
                  </Typography>
                </>
            }
        </>
    )
};

export default withRouter(PlaylistsByDuration);

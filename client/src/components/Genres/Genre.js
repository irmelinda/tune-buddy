import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    titleBar: {
        backgroundColor: 'transparent',
    },
    gridList: {
        width: 150,
        height: 150,
        listStyle: 'none'
    },
    image: {
        borderRadius:100,
        padding: 'none'
    },
}));
const Genre = ({ id, name, picture_medium_url, tracks_count }) => {
    const classes = useStyles();

    return (
        <>
            <GridListTile className={classes.gridList} key={id}>
                <img src={picture_medium_url} alt={name} className={classes.image} />
                <GridListTileBar
                    className={classes.titleBar}
                    title={name}
                />
            </GridListTile>
            <IconButton disabled color="inherit">
                <MusicNoteIcon />
                <Typography component="div" variant="body1" className={classes.title}>
                    {`${tracks_count} tracks`}
                </Typography>
            </IconButton>
        </>
    );
};

export default Genre;

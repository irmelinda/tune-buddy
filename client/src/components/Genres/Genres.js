import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Genre from '../Genres/Genre';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        justifyContent: 'left',
        border: 'none',
    },
    paper: {
        textAlign: 'center',
        backgroundColor: 'transparent',
        boxShadow: 'none'
    },
    grid: {
        justifyContent: 'space-between',
    },
}));

const Genres = ({ data }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container className={classes.grid}>
                {data && data.map(p => {
                    return (
                        <Paper className={classes.paper}>
                            <Genre
                                {...p}
                                key={p.id}
                            />
                        </Paper>
                    )
                })
                }
            </Grid>
        </div>
    );
}

export default Genres;

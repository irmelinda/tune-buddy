import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardMedia } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import httpUnsplash from '../../hooks/httpUnsplash';
import { useState } from 'react';
import Loader from '../Loader/Loader';

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: '0px'
  },
  image: {
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '285px',
    borderRadius: '0px'
  },
  paper: {
    margin: theme.spacing(4, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
  },
  submit: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white',
    '&:hover': {
      backgroundColor: 'white',
      color: 'black',
    },
  },
  submitOutlined: {
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    color: 'black'
  },
  imageIcon: {
    fill: 'orange',
 
  }
}));

const PersonalizePlaylist = ({ playlistName, handlePlaylistNameForm, cover, setCover, selectedFile, setSelectedFile }) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [errorUnsplash, setErrorUnsplash] = useState(false)
  const [message, setMessage] = useState(null);

  const handleRandomClick = (event) => {
    httpUnsplash(`https://api.unsplash.com/photos/random?client_id=CXFSdcaBUF5HbvtaQ6xPSDKrg0DOly4kXqt8ViIjRuA`, setCover, setErrorUnsplash, setLoading, setMessage);
  };
  const showImage = () => {
    const card =
      <Card className={classes.root}>
        <CardMedia
          className={classes.image}
          component="img"
          alt="Nothing selected"
          image={cover}
          title="Contemplative Reptile"
        />

      </Card>;

    if (message) {
      return <>{message}</>
    }
    if (errorUnsplash) {
      return <>Error from Unsplash</>
    }
    if (loading) {
      return <Loader />
    }
    return card;
  }

  return (
    <Grid container component="main">
      <CssBaseline />

      <Grid item xs={false} sm={4} md={7} >
        {showImage()}
      </Grid>

      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square >
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Personalize
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              error={!playlistName.valid}
              helperText={playlistName.valid ? null : <span>{playlistName.errorText}​​​​​​​​</span>}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label={playlistName.valid ? 'Playlist name' : 'Error'}
              name="email"
              autoComplete="email"
              autoFocus
              value={playlistName.value}
              onChange={handlePlaylistNameForm}
            />
            <Button
              fullWidth
              variant="contained"
              className={classes.submit}
              startIcon={<ReplayIcon className={classes.imageIcon} />}
              onClick={handleRandomClick}
            >
              RANDOM
            </Button>

          </form>

          {/* <form>
            <FileUploader
              setShowRandom={setShowRandom}
              onFileSelectSuccess={(file) => { setSelectedFile(file); console.log(file) }}
              onFileSelectError={({ error }) => alert(error)}
            />
          </form> */}

        </div>
      </Grid>
    </Grid >
  );
};
export default PersonalizePlaylist;

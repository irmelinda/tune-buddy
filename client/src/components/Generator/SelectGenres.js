import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Checkbox from '@material-ui/core/Checkbox';
import { FormControlLabel } from '@material-ui/core';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Slider from '@material-ui/core/Slider';
import Favorite from '@material-ui/icons/Favorite';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        marginTop: '25px'
    },
    colorPrimary: {
        color: 'black',
    },
    colorSecondary: {
        color: 'orange'
    }
}));

const SelectGenres = ({ valueSlider, setValueSlider, genres }) => {
    const classes = useStyles();
    const [checked, setChecked] = useState([]);

    const handleToggle = (value) => () => {

        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
            setValueSlider(valueSlider.map((el, i) => (i === value) ? { ...el, percent: 0 } : { ...el }));
        }
        newChecked.length <= 3 && setChecked(newChecked);
    };

    const handleSliderChange = (event, newValue, index) => {
        setValueSlider(valueSlider.map((el, i) => (i === index) ? { ...el, percent: newValue } : { ...el }));
    };
    return (
        <List>
            {genres.map((value, index) => {
                return (
                    <div key={index} className={classes.root}>
                        <Grid container spacing={3} alignItems="center">
                            <Grid item xs={3}>
                                <FormControlLabel
                                    disabled={checked.indexOf(index) === -1 && checked.length >= 3}
                                    control={
                                        <Checkbox
                                            onClick={handleToggle(index)}
                                            icon={<FavoriteBorder />}
                                            checkedIcon={<Favorite className={classes.colorSecondary} />}
                                            checked={checked.indexOf(index) !== -1 || valueSlider[index].percent !== 0}
                                            name="checkedD"
                                        />
                                    }
                                    label={value.name}
                                />
                            </Grid>
                            <Grid item xs={9}  >
                                {(checked.indexOf(index) !== -1 || valueSlider[index].percent !== 0) && (
                                    <Slider className={classes.colorPrimary}
                                        marks
                                        defaultValue={0}
                                        value={valueSlider[index].percent}
                                        aria-labelledby="discrete-slider-always"
                                        step={10}
                                        valueLabelDisplay="on"
                                        valueLabelFormat={() => ''}
                                        onChangeCommitted={(event, newValue,) => handleSliderChange(event, newValue, index)}
                                    />
                                )}
                            </Grid>
                        </Grid>
                    </div>
                );
            })}
            <div className={classes.root}>
                <Grid container spacing={3} alignItems="center">
                </Grid>
            </div>
        </List>
    );
}
export default SelectGenres;

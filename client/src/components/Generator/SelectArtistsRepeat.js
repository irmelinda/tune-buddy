import Checkbox from '@material-ui/core/Checkbox';
import { FormControlLabel } from '@material-ui/core';

const SelectArtistsRepeat = ({repeatArtists, setRepeatArtists}) => {

    const handleChange = (event) => {
        setRepeatArtists(event.target.checked)
    };

    return (
        <FormControlLabel
            control={
                <Checkbox
                    checked={repeatArtists}
                    onChange={handleChange}
                    name="checkedA"
                />}
            label='REPEAT ARTISTS'
        />
    );
}
export default SelectArtistsRepeat;

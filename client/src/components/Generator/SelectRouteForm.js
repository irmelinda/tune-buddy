import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Card, CardMedia, Grid } from '@material-ui/core';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Paper from '@material-ui/core/Paper';
import image from '../../common/wallpaper/road-trip-01.png'
import autoSuggest from '../../hooks/autoSuggest'
import calculateRoute from '../../hooks/calculateRoute';

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: '0px'
  },
  image: {
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '310px',
    borderRadius: '0px'
  },
  paper: {
    margin: theme.spacing(4, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
  },
  imageIcon: {
    fill: 'white'
  }
}));

const toTime = (seconds) => {
  var date = new Date(null);
  date.setSeconds(seconds);
  return date.toISOString().substr(11, 8);
}

const SelectRouteForm = (props) => {
  const { setIfError, setTripDuration, tripDuration, valueStart, setValueStart, valueEnd, setValueEnd } = props;
  const classes = useStyles();
  const [inputValueStart, setInputValueStart] = useState('')
  const [inputValueEnd, setInputValueEnd] = useState('')
  const [newSuggestionStart, setNewSuggestionStart] = useState([]);
  const [newSuggestionEnd, setNewSuggestionEnd] = useState([]);
  const [message, setMessage] = useState('');

  return (
    <>
      <Grid container component="main">
        <CssBaseline />

        <Grid item xs={false} sm={4} md={7}>
          <Card className={classes.root}>
            <CardMedia
              className={classes.image}
              component="img"
              alt="Nothing selected"
              image={image}
              title="Contemplative Reptile"
            />
          </Card>
        </Grid>

        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square >
          <div className={classes.paper}>
            <Typography component="h1" variant="h6">
              Select Destinations
          </Typography>
            <form className={classes.form} noValidate>
              <Autocomplete
                id="start-point"
                freeSolo
                options={newSuggestionStart.map((s) => s.address.formattedAddress)}
                inputValue={inputValueStart}
                onInputChange={(event, newInputValue) => {
                  setInputValueStart(newInputValue);
                  newInputValue && autoSuggest(newInputValue, setNewSuggestionStart);
                }}

                value={valueStart}
                onChange={(event, newValue) => {
                  setValueStart(newValue);
                  (newValue && valueEnd) && calculateRoute(newValue, valueEnd, setTripDuration, setIfError, setMessage);
                  !newValue && setIfError(true)
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    margin="normal"
                    id="startPoint"
                    label="From"
                    name="startPoint"
                  />
                )}
              />

              <ArrowForwardIcon />

              <Autocomplete
                id="end-point"
                freeSolo
                options={newSuggestionEnd.map((s) => s.address.formattedAddress)}
                inputValue={inputValueEnd}
                onInputChange={(event, newInputValue) => {
                  setInputValueEnd(newInputValue);
                  newInputValue && autoSuggest(newInputValue, setNewSuggestionEnd);
                }}

                value={valueEnd}
                onChange={(event, newValue) => {
                  setValueEnd(newValue);
                  (valueStart && newValue) && calculateRoute(valueStart, newValue, setTripDuration, setIfError, setMessage)
                  !newValue && setIfError(true)
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    margin="normal"
                    id="endPoint"
                    label="To"
                    name="endPoint"
                  />
                )}
              />
            </form>
            {(message && valueStart && valueEnd) ? <span className={classes.error} >{message}</span> : null}
            {(valueStart === null || valueEnd === null) ? <span className={classes.error} >Missing or invalid destination</span> : null}
            {(tripDuration && valueStart && valueEnd && !message) ? <span className={classes.error} >Travel duration is {toTime(tripDuration)} hours</span> : null}
          </div>
        </Grid>
      </Grid >
    </>
  );
}

export default SelectRouteForm;

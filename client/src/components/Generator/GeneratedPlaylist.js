import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button, Card, CardMedia, Checkbox, CssBaseline, FormControlLabel, Grid, Typography } from "@material-ui/core";
import tracksContext from '../../providers/tracksContext';
import generateTracks from '../../hooks/generateTracks';
import { Favorite, FavoriteBorder } from '@material-ui/icons';
import constants from '../../common/constants';
import { withRouter } from 'react-router';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    verticalAlign: 'middle'
  },
  table: {
    minWidth: 650,
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white',
    '&:hover': {
      backgroundColor: 'white',
      color: 'black',
    },
  },
  header: {
    paddingTop: 20,
    paddingLeft: 15,

  },
  nav: {

  },
  submitOutlined: {
    marginTop: theme.spacing(2),
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'white',
    color: 'black',
    '&:hover': {
      backgroundColor: 'black',
      color: 'white',
    },
  },
  red: {
    marginTop: theme.spacing(2),
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'orange',
    color: 'black',
    '&:hover': {
      backgroundColor: 'black',
      color: 'orange',
    },
  },
  paper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  image: {
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '285px',
  },
  colorSecondary: {
    color: 'orange'
  }
}));

const toTime = (seconds) => {
  var date = new Date(null);
  date.setSeconds(seconds);
  return date.toISOString().substr(11, 8);
}

const genres = [
  {
    genres_id: 85,
    name: 'Alternative'
  },
  {
    genres_id: 106,
    name: 'Electro'
  },
  {
    genres_id: 116,
    name: 'Rap/Hip Hop'
  },
  {
    genres_id: 129,
    name: 'Jazz'
  },
  {
    genres_id: 152,
    name: 'Rock'
  },
  {
    genres_id: 169,
    name: 'Soul & Funk'
  },
];

const GeneratedPlaylist = (props) => {
  const classes = useStyles();
  const { valueSlider,
    tripDuration,
    repeatArtists,
    setMessage,
    setError,
    cover,
    playlistName,
    setSnackbarMessage,
    handleClickForSnackbar } = props;

  const { tracksValue, setTracksValue } = useContext(tracksContext)

  const handleGenerateTracks = () => {
    generateTracks(valueSlider, tripDuration, repeatArtists, setMessage, setError, setTracksValue, tracksValue);
  }
  const [saveClicked, setSaveClicked] = useState(false);
  const newPercent = (value, sum) => {
    return Math.round(100 / sum * value);
  }
  const sum = valueSlider.reduce((acc, el) => acc + el.percent, 0)
  const formattedValueSlider = valueSlider
    .filter(el => el.percent !== 0)
    .map(el => ({ ...el, percent: newPercent(el.percent, sum) }));

  const handleSaveClicked = () => {
    const token = localStorage.token;

    fetch(`${constants.BASE_URL}/playlists`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: playlistName.value,
        cover_url: cover,
        tracks_list: tracksValue.map(el => el.id),
      })
    })
      .then(response => response.json())
      .then(({ data, message, tracks }) => {

        if (!data) {
          setMessage(message);
        }
      })
      .catch(error => setError(error));
    setSaveClicked(true);
    setSnackbarMessage('Successfully created. Redirecting to home page.');
    handleClickForSnackbar();

    props.history.push(`/my-account/my-playlists`);
  };

  console.log()

  return (
    <>
      <Grid container component="main">
        <CssBaseline />

        <Grid item xs={false} sm={8} md={5} >
          <Card className={classes.root}>
            <CardMedia
              className={classes.image}
              component="img"
              alt="Nothing selected"
              image={cover}
              title="Contemplative Reptile"
            />

          </Card>
        </Grid>

        <Grid item xs={12} sm={4} md={7} component={Paper} square >
          <div className={classes.paper}>
            <Typography component="div" variant="h5">
              {playlistName.value}
            </Typography>

            {repeatArtists
              ? <FormControlLabel
                control={
                  <Checkbox
                    disabled
                    checked={repeatArtists}

                    name="checkedA"
                  />}
                label='REPEAT ARTISTS'
              /> :
              null}

            {formattedValueSlider.length === 0 && 'No selected genres'}
            {formattedValueSlider.map(el => <Typography key={el.genres_id} component="div" variant="body1">
              <Checkbox
                disabled

                icon={<FavoriteBorder />}
                checkedIcon={<Favorite className={classes.colorSecondary} />}
                checked={el.percent}
                name="checkedD"
              />
              {`${el.percent} % `}
              {genres.map(e => (e.genres_id === el.genres_id) && e.name)}
            </Typography>
            )}


          </div>
        </Grid>

        <Grid item xs={12} sm={12} md={12} component={Paper} square >
          <div className={classes.paper}>
            <Typography component="div" variant="h5">
              Generated Tracks
            </Typography>
            <TableContainer component={Paper}>
              <div style={{ height: 250, width: '100%' }}>

                <Table className={classes.table} size="small" aria-label="a dense table" >
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="left">#</TableCell>
                                        <TableCell align="left">TRACK</TableCell>
                                        <TableCell align="left">ARTIST</TableCell>
                                        <TableCell align="left">ALBUM</TableCell>
                                        <TableCell align="left">DURATION</TableCell>
                                        <TableCell align="left">GENRE</TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {tracksValue && tracksValue.map((row, i) => (
                                        <TableRow key={i}>
                                            <TableCell component="th" scope="row" >{i}</TableCell>
                                            <TableCell sizeSmall align="left" size="medium">{row.title}</TableCell>
                                            <TableCell align="left">{row.name}</TableCell>
                                            <TableCell align="left">{row.album_title}</TableCell>
                                            <TableCell align="left" size="small">{toTime(row.duration)}</TableCell>
                                            <TableCell align="left" size="medium">{row.genre_name}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                
              </div>
            </TableContainer>
            <Button
              variant="contained"
              onClick={handleGenerateTracks}
              className={classes.submitOutlined}>
              NEW TRACKS
          </Button>
          </div>
        </Grid>
      </Grid >
      {!saveClicked
        ? <Button
          onClick={handleSaveClicked}
          variant="contained"
          color="primary"
          className={classes.red}>
          SAVE
      </Button>
        : null
      }
    </>
  )
}
export default withRouter(GeneratedPlaylist);


import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { Button, TextField, Typography } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { useContext } from 'react';
import AuthContext from '../../providers/authContext';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import constants from '../../common/constants';
import ScheduleIcon from '@material-ui/icons/Schedule';

const useStyles = makeStyles((theme) => ({
  titleBar: {
    backgroundColor: 'transparent',
  },
  gridList: {
    width: 200,
    height: 200,
    listStyle: 'none'
  },
  image: {
    padding: 'none'
  },
  more: {
    color: 'orange',
    paddingLeft: 2
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white',
    '&:hover': {
      backgroundColor: 'white',
      color: 'black',
    },
  },
  form: {
    padding: theme.spacing(3),
    minWidth: '400px'
  }
}));

const toTime = (seconds) => {
  var date = new Date(null);
  date.setSeconds(seconds);

  return date.toISOString().substr(11, 8);
}

const Playlist = (props) => {
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const authUser = useContext(AuthContext);
  const { id, cover_url, name, tracks_avg_rank, duration, goToOurPicksSingle, users_id, setData, data, showDeleteEdit } = props;
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null)
  const [message, setMessage] = useState(null);
  const [currentImage, setCurrentImage] = useState(cover_url);
  const [ifError, setIfError] = useState(false);

  const [playlistName, setPlaylistName] = useState({
    value: name,
    valid: true,
    validate: value => value && value.length > constants.nameMinLength,
    errorText: `Name should be at least 3 characters.`,
  });

  const handlePlaylistNameForm = event => {
    playlistName.value = event.target.value;
    playlistName.valid = playlistName.validate(event.target.value);
    playlistName.touched = true;
    setPlaylistName({ ...playlistName });

    playlistName.valid && setIfError(false);
    !playlistName.valid && setIfError(true);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickOpenEdit = () => {
    setOpenEdit(true);
  }

  const handleClose = () => {
    setOpen(false);
    setOpenEdit(false);
  };

  const handleDelete = () => {
    setOpen(false);

    const token = localStorage.token;
    setLoading(true);

    fetch(`${constants.BASE_URL}/playlists/${id}`, {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(({ data: dataRes, message }) => {

        if (!dataRes) {
          setMessage(message);
        }
        const index = data.findIndex((el) => el.id === id);
        const updatedData = data.slice();
        updatedData.splice(index, 1);
        setData(updatedData);

      })
      .catch(error => setError('error'))
      .finally(() => setLoading(false));

  }

  const handleEdit = () => {
    setOpenEdit(false);

    const token = localStorage.token;
    setLoading(true);

    fetch(`${constants.BASE_URL}/playlists/${id}`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: playlistName.value,
      })
    })
      .then(response => response.json())
      .then(({ data: dataRes, message }) => {

        if (!dataRes) {
          setMessage(message);
        }
        setData(data.map(el => el.id === id ? { ...el, name: playlistName.value } : el))

      })
      .catch(error => setError('error'))
      .finally(() => setLoading(false));
  }

  return (
    <>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Delete"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {name}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            color="primary"
            variant="contained"
            className={classes.button}>
            CANCEL
          </Button>
          <Button onClick={handleDelete} color="primary"
            variant="contained"
            className={classes.button}>
            DELETE
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={openEdit}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Edit"}</DialogTitle>

        <form noValidate className={classes.form}>
          <TextField

            error={!playlistName.valid}
            helperText={playlistName.valid ? null : <span>{playlistName.errorText}​​​​​​​​</span>}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label={playlistName.valid ? 'Playlist name' : 'Error'}
            name="email"
            autoComplete="email"
            autoFocus
            value={playlistName.value}
            onChange={handlePlaylistNameForm}
          />
        </form>
        <DialogActions>

          <Button
            onClick={handleClose}
            color="primary"
            variant="contained"
            className={classes.button}>
            CANCEL
          </Button>
          <Button onClick={handleEdit} color="primary"
            variant="contained"
            className={classes.button}>
            EDIT
          </Button>

        </DialogActions>
      </Dialog>

      <GridListTile className={classes.gridList} key={id}>
        <img src={cover_url} alt={name} className={classes.image} />
        <GridListTileBar
          className={classes.titleBar}
          title={name}
          subtitle={<Typography component="div" variant="body1" className={classes.title}>
            <ScheduleIcon />
            {toTime(duration)}
          </Typography>}
          actionIcon={
            <IconButton className={classes.more} onClick={goToOurPicksSingle}>
              <VisibilityIcon />
            </IconButton>
          }
        />
      </GridListTile>

      <IconButton disabled color="inherit">
        <FavoriteIcon />
        <Typography component="div" variant="body1" className={classes.title}>
          {Math.floor(tracks_avg_rank)}
        </Typography>
      </IconButton>

      {(users_id === authUser.user.sub && showDeleteEdit)
        ?
        <>
          <IconButton color="inherit" onClick={handleClickOpen}>
            <DeleteIcon />
          </IconButton>
          <IconButton color="inherit" onClick={handleClickOpenEdit}>
            <EditIcon />
          </IconButton>
        </>
        :
        null
      }
    </>
  );
}

export default Playlist;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  titleBar: {
    backgroundColor: 'transparent',
  },
  gridList: {
    width: 200,
    height: 200,
    listStyle: 'none'
  },
  image: {
    padding: 'none'
  },
  more: {
    color: 'black',
  },
}));

const toTime = (seconds) => {
  var date = new Date(null);
  date.setSeconds(seconds);

  return date.toISOString().substr(11, 8);
}

const Playlist = (props) => {
  const { id, cover_url, name, tracks_avg_rank, duration, goToOurPicksSingle } = props;
  const classes = useStyles();

  return (
    <>
      <GridListTile className={classes.gridList} key={id}>
        <img src={cover_url} alt={name} className={classes.image} />
        <GridListTileBar
          className={classes.titleBar}
          title={name}
          subtitle={`${toTime(duration)}`}
        />
      </GridListTile>

      <IconButton className={classes.more} onClick={goToOurPicksSingle}>
        <VisibilityIcon />
      </IconButton>

      <IconButton disabled color="inherit">
        <FavoriteIcon />
        <Typography component="div" variant="body1" className={classes.title}>
          {Math.floor(tracks_avg_rank)}
        </Typography>
      </IconButton>
    </>
  );
}

export default Playlist;

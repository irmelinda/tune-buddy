import React from 'react'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
    custom: {
        backgroundColor: 'black'
    }
}));

const CustomizedSnackbar = ({ handleCloseSnackbar, openSnackbar, snackbarMessage }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Snackbar anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }} open={openSnackbar} autoHideDuration={3000} onClose={handleCloseSnackbar}>
                <Alert onClose={handleCloseSnackbar} className={classes.custom}>
                    {snackbarMessage}
                </Alert>
            </Snackbar>
        </div>
    );
};

export default CustomizedSnackbar;

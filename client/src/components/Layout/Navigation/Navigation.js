import { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import LibraryMusicIcon from '@material-ui/icons/LibraryMusic';
import AlbumIcon from '@material-ui/icons/Album';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Zoom from '@material-ui/core/Zoom';
import PropTypes from 'prop-types';
import { Avatar, CssBaseline, Divider, Fab } from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Box from '@material-ui/core/Box';
import CloseIcon from '@material-ui/icons/Close';
import { NavLink, withRouter } from 'react-router-dom';
import AuthContext from '../../../providers/authContext';
import constants from '../../../common/constants';
import LockRoundedIcon from '@material-ui/icons/LockRounded';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import CustomizedSnackbar from '../../Snackbar/Snackbar';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';

const useStyles = makeStyles((theme) => ({
    toolbar: {
        backgroundColor: 'green',
        minHeight: '0px',
    },
    box: {
        // backgroundColor: 'grey',
        minHeight: 'calc(100vh - 64px - 56px)',
        marginBottom: '0px',
        marginTop: '0px',
        paddingTop: '0px',
        paddingBottom: '0px',

    },
    root: {
        position: 'fixed',
        bottom: theme.spacing(9),
        right: theme.spacing(2),

    },
    customColor: {
        backgroundColor: 'black',
        color: 'white',
        height: '60px'
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        color: 'white',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
}));

function ScrollTop(props) {
    const { children, window } = props;
    const classes = useStyles();
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector('#back-to-top-anchor');

        if (anchor) {
            anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
        }
    };

    return (
        <Zoom in={trigger}>
            <div onClick={handleClick} role="presentation" className={classes.root}>
                {children}
            </div>
        </Zoom>
    );
}

ScrollTop.propTypes = {
    children: PropTypes.element.isRequired,
    window: PropTypes.func,
};

const Navigation = (props) => {
    const authUser = useContext(AuthContext);
    const classes = useStyles();
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
    //snackbar state
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [openSnackbar, setOpenSnackbar] = useState(false);

    //handle state snackbar
    const handleClickForSnackbar = () => {
        setOpenSnackbar(true);
    };
    const handleCloseSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnackbar(false);
    };

    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const logout = () => {
        const token = localStorage.getItem('token')
        localStorage.removeItem('token');
        fetch(`${constants.BASE_URL}/users/logout`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "authorization": "Bearer " + token,
            }
        })
        authUser.setAuthState({
            user: null,
            isLoggedIn: false,
        });
        setSnackbarMessage('You have successfully logged out');
        handleClickForSnackbar();
        props.history.push(`/home`)
    };

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMobileMenuOpen = (event) => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const menuId = 'custom-tune-buddy-nav';

    const mobileMenuId = 'custom-tune-buddy-nav-mobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
            <MenuItem onClick={handleMobileMenuClose}>
                <IconButton color="inherit">
                    <Badge color="secondary">
                        <CloseIcon />
                    </Badge>
                </IconButton>
                <p></p>
            </MenuItem>


            {authUser.isLoggedIn && authUser.user.role === 'admin' ?
                <div>
                    <Divider />
                    <NavLink to="/admin">
                        <MenuItem onClick={handleMobileMenuClose}>
                            <IconButton color="inherit">
                                <Badge color="secondary">
                                    <SupervisorAccountIcon />
                                </Badge>
                            </IconButton>
                            <p>ADMIN PANEL</p>
                        </MenuItem>
                    </NavLink>
                </div>
                : null
            }

            <Divider />
            {/* <NavLink to="/playlists">
                <MenuItem onClick={handleMobileMenuClose}>
                    <IconButton color="inherit">
                        <Badge color="secondary">
                            <LibraryMusicIcon />
                        </Badge>
                    </IconButton>
                    <p>PLAYLISTS</p>
                </MenuItem>
            </NavLink> */}

            <NavLink to="/generator">
                <MenuItem onClick={handleMobileMenuClose}>
                    <IconButton color="inherit">
                        <Badge color="secondary">
                            <AlbumIcon />
                        </Badge>
                    </IconButton>
                    <p>GENERATOR</p>
                </MenuItem>
            </NavLink>

            <Divider />
            <MenuItem >
                <p>MY ACCOUNT</p>
            </MenuItem>

            {/* <NavLink to="/my-account/profile"> */}
                <MenuItem onClick={handleMobileMenuClose}>
                    <IconButton color="inherit">
                        <Badge color="secondary">
                            <AccountCircleIcon />
                        </Badge>
                    </IconButton>
                    <p>PROFILE</p>
                </MenuItem>
            {/* </NavLink> */}

            <NavLink to="/my-account/my-playlists">
                <MenuItem onClick={handleMobileMenuClose}>
                    <IconButton color="inherit">
                        <Badge color="secondary">
                            <LibraryMusicIcon />
                        </Badge>
                    </IconButton>
                    <p>MY PLAYLISTS</p>
                </MenuItem>
            </NavLink>

            <Divider />
            <MenuItem onClick={() => { handleMobileMenuClose(); logout() }}>
                <IconButton
                    aria-label="account of current user"
                    aria-controls="custom-tune-buddy-nav"
                    aria-haspopup="true"
                    color="inherit"
                >
                    <LockRoundedIcon />
                </IconButton>
                <p>LOGOUT</p>
            </MenuItem >
        </Menu>
    );

    return (
        <>
            <CustomizedSnackbar
                handleCloseSnackbar={handleCloseSnackbar}
                openSnackbar={openSnackbar}
                snackbarMessage={snackbarMessage} />
            <CssBaseline />
            <div className={classes.grow}>
                <AppBar position="static">
                    <Toolbar className={classes.customColor}>
                        <NavLink to="/home">
                            <Typography className={classes.title} variant="h6" noWrap>
                                TUNE BUDDY
                        </Typography>
                        </NavLink>
                        <div className={classes.grow} />
                        <div className={classes.sectionDesktop}>

                            {authUser.isLoggedIn && authUser.user.role === 'admin' ?
                                <NavLink to="/admin" >
                                    <MenuItem className={classes.customColor}>
                                        <IconButton color="inherit">
                                            <Badge color="secondary">
                                                <SupervisorAccountIcon />
                                            </Badge>
                                        </IconButton>
                                        <p>ADMIN PANEL</p>
                                    </MenuItem>
                                </NavLink>
                                : null
                            }

                            {authUser.isLoggedIn &&
                                <NavLink to="/my-account/my-playlists" >
                                    <MenuItem className={classes.customColor}>
                                        <IconButton color="inherit">
                                            <Badge color="secondary">
                                                <LibraryMusicIcon />
                                            </Badge>
                                        </IconButton>
                                        <p>MY PLAYLISTS</p>
                                    </MenuItem>
                                </NavLink>
                            }
                            {authUser.isLoggedIn &&
                                <NavLink to="/generator" >
                                    <MenuItem className={classes.customColor}>
                                        <IconButton color="inherit">
                                            <Badge color="secondary">
                                                <AlbumIcon />
                                            </Badge>
                                        </IconButton>
                                        <p>GENERATOR</p>
                                    </MenuItem>
                                </NavLink>
                            }
                            {authUser.isLoggedIn
                                ? <MenuItem onClick={logout} className={classes.customColor}>
                                    <p>LOGOUT</p>
                                </MenuItem >
                                : <NavLink to="/login">
                                    <MenuItem className={classes.customColor}>
                                        <p>LOGIN</p>
                                    </MenuItem>
                                </NavLink>
                            }
                            {authUser.isLoggedIn &&
                                <NavLink to="/my-account/profile">
                                    <IconButton
                                        edge="end"
                                        aria-label="account of current user"
                                        aria-controls={menuId}
                                        aria-haspopup="true"
                                        color="inherit"
                                    >
                                        <Avatar
                                            alt="auth.user.username"
                                            src={authUser.user.avatar_url}
                                            className={classes.medium} />
                                    </IconButton>
                                 </NavLink>
                            }

                        </div>
                        <div className={classes.sectionMobile}>
                            {authUser.isLoggedIn

                                ? <IconButton
                                    aria-label="show more"
                                    aria-controls={mobileMenuId}
                                    aria-haspopup="true"
                                    onClick={handleMobileMenuOpen}
                                    color="inherit"
                                >
                                    <MenuIcon />
                                </IconButton>
                                : <NavLink to="/login">
                                    <MenuItem className={classes.customColor}>
                                        <p>LOGIN</p>
                                    </MenuItem>
                                </NavLink>
                            }
                        </div>
                    </Toolbar>
                </AppBar>
                {renderMobileMenu}
            </div>
            <Toolbar id="back-to-top-anchor" className={classes.toolbar} />
            <Box my={2} className={classes.box}>
                {props.children}
            </Box>
            <ScrollTop {...props}>
                <Fab color="secondary" size="small" aria-label="scroll back to top">
                    <KeyboardArrowUpIcon />
                </Fab>
            </ScrollTop>

        </>
    );
};

export default withRouter(Navigation);

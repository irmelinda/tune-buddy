import { Fragment } from "react";
import Footer from "../Footer/Footer";
import Navigation from '../Navigation/Navigation';

const Layout = props => {

    return (
        <Fragment>
            <Navigation>
            {props.children}
            </Navigation>
            <Footer />
        </Fragment>
    )
}

export default Layout;

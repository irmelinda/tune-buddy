import React from 'react';
import constants from '../../common/constants';
import useHttp from '../../hooks/useHttp';
//import ProfilePictureForm from './ProfilePictureForm';
//import ProfileDataForm from './ProfileDataForm';
//import usersContext from '../../providers/usersContext'
import { withRouter } from "react-router";
//import CustomizedSnackbar from '../../components/Snackbar/Snackbar';
import ProfileForm from './MyAccountForm';


const MyAccount = (props) => {

  const { data, loading, message, error } = useHttp(
    `${constants.BASE_URL}/users/my-account`, []
  );
  if (loading) {
    return <>Loading...</>;
  }
  if (message) {
    return <>Message...</>;
  }
  if (error) {
    return <>Error...</>;
  }

  const goToMyPlaylist = () => {
    props.history.push('/my-account/my-playlists');
  }

  return (
    <>
      <ProfileForm
        data={data}
        goToMyPlaylist={goToMyPlaylist}
      />
    </>
  )
};

export default withRouter(MyAccount);

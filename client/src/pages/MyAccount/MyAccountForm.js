//import Wallpaper from '../../components/Common/Wallpaper/Wallpaper'
import { Fragment } from "react";
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './MyAccount.css'
import { Avatar, Button, Container, Grid, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
    paddingBottom: 20,
    paddingTop: 50,
  },

  title: {
    paddingTop: 20,
    paddingBottom: 15,
    paddingLeft: 15,
  },
  large: {
    height: 150,
    width: 150,
  },
  myPlaylistBtn: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white',
    '&:hover': {
        backgroundColor: 'lightgrey',
        color: 'black',
    },
    "disabledButton": {
        backgroundColor: theme.palette.primary || 'white'
    }
},
}));

const MyAccountForm = (props) => {
  const { data, goToMyPlaylist } = props;
  const classes = useStyles();
  function createData(name, data) {
    return { name, data };
  }

  const rows = [
    createData('Username', data.username),
    createData('Name', data.name),
    createData('Email', data.email),
  ];

  return (
    <Container maxWidth="md">

      <Grid container spacing={3}>

        <Grid item xs={12}>
          <Typography className={classes.title} component="h1" variant="h5">
            Profile overview
            </Typography>
        </Grid>


        <Grid item xs={5}>
          {!data.avatar_url
            ? <Avatar src="/broken-image.jpg" className={classes.large} />
            : <Avatar src={data.avatar_url} className={classes.large} />}
        </Grid>

        <Grid item xs container direction="column" spacing={5}>
          <Button fullWidth onClick={goToMyPlaylist} className={classes.myPlaylistBtn} variant="outlined">
            My Playlists
          </Button>
        
        </Grid>

        <Grid item xs={12}>
          <Typography className={classes.title} component="h3" variant="h5">
            Personal data
            </Typography>
        </Grid>

        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableBody>
                  {rows.map((row) => (
                    <TableRow key={row.name}>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell align="left">{row.data}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Grid>
      </Grid >
    </Container >

  )
};
export default MyAccountForm;

import Button from '@material-ui/core/Button';
import Playlists from '../../components/Playlists/Playlists';
import constants from '../../common/constants';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Typography } from '@material-ui/core';
import { withRouter } from 'react-router';
import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/authContext';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2),
        margin: theme.spacing(2, 0, 2),
        borderRadius: '50px',
        backgroundColor: 'orange',
        color: 'black',
        '&:hover': {
            backgroundColor: 'black',
            color: 'orange',
        },
    },
    title: {
        padding: theme.spacing(4, 0, 2),
        color: 'black'
    },
    row: {
        direction: 'row'
    }
}));

const Home = (props) => {
    const authUser = useContext(AuthContext);
    const classes = useStyles();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null)
    const [message, setMessage] = useState(null);
    const showDeleteEdit = true;

    useEffect(() => {
        setLoading(true);
        const token = localStorage.token;
        let mounted = true;

        fetch(`${constants.BASE_URL}/playlists?playlistsCount=500&orderBy=created_on&users_id=${authUser.user.sub}`, {
            headers: {
                authorization: `Bearer ${token}`
            },
        })
            .then(response => response.json())
            .then(({ data, message }) => {
                if (mounted) {
                    if (!data) {
                        setMessage(message);
                    }

                    setData(data);
                }
            })
            .catch(error => setError('error'))
            .finally(() => setLoading(false))

        const cleanup = () => {
            mounted = false;
        }

        return cleanup;
    }, []);

    if (loading) {
        return <>Loading...</>;
    }

    return (
        <div className={classes.root}>
            <Container component="main" maxWidth="lg" className={classes.row}>
                <CssBaseline />
                <div className={classes.paper}>
                    

                    {(data.filter(p => p.is_deleted !== 1).length>0) && <>
                        <Typography component="div" variant="h6" className={classes.title}>
                            MY PLAYLISTS
                            <Button
                                    variant="contained"
                                    className={classes.button}
                                    onClick={() => props.history.push(`/generator`)}>GENERATE NEW</Button>
                            
                        </Typography>
                        
                        <Playlists props={props} data={data} setData={setData} showDeleteEdit={showDeleteEdit} />

                    </>

                    }
                    {(message || data.filter(p => p.is_deleted !== 1).length===0) &&
                        <>
                            <Typography component="div" variant="h6" className={classes.title}>
                                NO CUSTOM PLAYLISTS
                            <Button
                                    variant="contained"
                                    className={classes.button}
                                    onClick={() => props.history.push(`/generator`)}>GENERATE FIRST</Button>
                            </Typography>
                            <Playlists props={props} />
                        </>

                    }

                </div>
            </Container>
        </div>
    )
};

export default withRouter(Home);

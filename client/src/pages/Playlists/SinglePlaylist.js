import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import constants from '../../common/constants';
import { Button, Container, CssBaseline, Grid, Typography } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import { useEffect, useState } from 'react';
import useHttp from '../../hooks/useHttp';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2),
        margin: theme.spacing(2, 0, 2),
        borderRadius: '50px',
        backgroundColor: 'orange',
        color: 'black',
        '&:hover': {
            backgroundColor: 'black',
            color: 'orange',
        },
    },
    title: {
        padding: theme.spacing(4, 0, 2),
        color: 'black'
    },
    root: {
        flexGrow: 1,
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
    },
    paper: {
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    table: {
        minWidth: 650,
    },
    header: {
        paddingTop: 20,
        paddingLeft: 15,

    },
    nav: {

    },
    submitOutlined: {
        marginTop: theme.spacing(2),
        margin: theme.spacing(2, 0, 2),
        borderRadius: '50px',
        backgroundColor: 'white',
        color: 'black',
        '&:hover': {
            backgroundColor: 'black',
            color: 'white',
        },
    },
    red: {
        marginTop: theme.spacing(2),
        margin: theme.spacing(2, 0, 2),
        borderRadius: '50px',
        backgroundColor: 'orange',
        color: 'black',
        '&:hover': {
            backgroundColor: 'black',
            color: 'orange',
        },
    },
    colorSecondary: {
        color: 'orange'
    },
    titleBar: {
        backgroundColor: 'transparent',
    },
    gridList: {
        width: 200,
        height: 200,
        listStyle: 'none'
    },
    image: {
        padding: 'none'
    },
}));

const toTime = (seconds) => {
    var date = new Date(null);
    date.setSeconds(seconds);
    return date.toISOString().substr(11, 8);
}

const SinglePlaylist = props => {
    const [data, setData] = useState({});
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null)
    const [message, setMessage] = useState(null);
    const { id } = props.match.params;
    const classes = useStyles();


    useEffect(() => {
        const token = localStorage.token;
        setLoading(true);
        let mounted = true;

        const url = token ? `${constants.BASE_URL}/playlists/${id}` : `${constants.BASE_URL}/public/${id}`

        fetch(url, {
            headers: {
                authorization: `Bearer ${token}`
            },
        })
            .then(response => response.json())
            .then(({ data, message }) => {
                if (mounted) {
                    if (!data) {
                        setMessage(message);
                    }
                    setData(data[0]);
                }
            })
            .catch(error => setError('error'))
            .finally(() => setLoading(false));

        const cleanup = () => {
            mounted = false;
        }

        return cleanup;

    }, [id]);

    const token = localStorage.token;
    const url = token ? `${constants.BASE_URL}/playlists/${id}/tracks` : `${constants.BASE_URL}/public/${id}/tracks`

    const { data: dataTracks, loadingTracks, messageTracks, errorTracks } = useHttp(url, []);

    if (loading || loadingTracks) {
        return <>Loading...</>;
    }

    if (message || messageTracks) {
        return <>Message...</>;
    }

    if (error || errorTracks) {
        return <>Error...</>;
    }

    return (
        <div className={classes.root}>

            <Container component="main" maxWidth="lg">
                <CssBaseline />

                <Typography component="div" variant="h6" className={classes.title}>
                    {data.name}
                </Typography>

                <div className={classes.paper}>

                    <GridListTile className={classes.gridList} key={data.id}>
                        <img src={data.cover_url} alt={data.name} className={classes.image} />
                        <GridListTileBar
                            className={classes.titleBar}
                        />
                    </GridListTile>

                    <IconButton disabled color="inherit">
                        <MusicNoteIcon />
                        <Typography component="div" variant="body1" className={classes.title}>
                            {data.tracks_count ? `${data.tracks_count} tracks` : '0 tracks'}
                        </Typography>
                    </IconButton>

                    <Typography component="div" variant="h6">
                        {/* {toTime(data.duration)} */}
                    </Typography>
                    <Typography component="div" variant="body1">
                        TRACKS AVERAGE RANK_
                        {Math.floor(data.tracks_avg_rank)}
                    </Typography>
                    <Typography component="div" variant="body1">
                        CREATED BY_
                      {data.username}
                    </Typography>

                    <Typography component="div" variant="body1">
                        TRACKS LIST
                    </Typography>
                    <Grid item xs={12} sm={12} md={12}  >
                        <TableContainer >

                            <Table className={classes.table} size="small" aria-label="a dense table" >
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="left">#</TableCell>
                                        <TableCell align="left">TRACK</TableCell>
                                        <TableCell align="left">ARTIST</TableCell>
                                        <TableCell align="left">ALBUM</TableCell>
                                        <TableCell align="left">DURATION</TableCell>
                                        <TableCell align="left">GENRE</TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {dataTracks && dataTracks.map((row, i) => (
                                        <TableRow key={i}>
                                            <TableCell component="th" scope="row" >{i}</TableCell>
                                            <TableCell sizeSmall align="left" size="medium">{row.title}</TableCell>
                                            <TableCell align="left">{row.name}</TableCell>
                                            <TableCell align="left">{row.album_title}</TableCell>
                                            <TableCell align="left" size="small">{toTime(row.duration)}</TableCell>
                                            <TableCell align="left" size="medium">{row.genre_name}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer >
                    </Grid >


                </div>
            </Container>
        </div>
    )
}

export default SinglePlaylist;


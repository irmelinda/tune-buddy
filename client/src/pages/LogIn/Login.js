import constants from "../../common/constants";
import decode from 'jwt-decode';
import AuthContext from '../../providers/authContext'
import { useContext, useState } from "react";
import { withRouter } from "react-router";
import LoginForm from "./LogInForm.js";
import CustomizedSnackbar from '../../components/Snackbar/Snackbar';


const Login = (props) => {
    const [invalidData, setInvalidData] = useState(false);
    const [error, setError] = useState('');
    const [user, setUser] = useState({
        email: {
            value: '',
            valid: true,
            validate: (value) => value && constants.emailRegex.test(value),
            errorText: `Invalid email format.`
        },
        password: {
            value: '',
            valid: true,
            validate: (value) => value !== undefined && value.length > constants.userPasswordMinLength && constants.passwordRegex.test(value),
            errorText: `Password should contain letters and at least 1 digit`
        },
    });


    //snackbar state
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [openSnackbar, setOpenSnackbar] = useState(false);

    //handle state snackbar
    const handleClickForSnackbar = () => {
        setOpenSnackbar(true);
    };
    const handleCloseSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnackbar(false);
    };
    
    const handleChangeForm = name => event => {
        user[name].value = event.target.value;
        user[name].valid = user[name].validate(event.target.value);
        user[name].touched = true;
        setUser({ ...user });
    };

    const auth = useContext(AuthContext);

    const redirect = () => setTimeout(() => {
        props.history.push('/home');
    }, 800);

    const login = (userRequest) => {

        fetch(`${constants.BASE_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(userRequest)
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message)
                } else {
                    const user = decode(result.token);
                    localStorage.setItem('token', result.token);
                    auth.setAuthState({ user, isLoggedIn: true });
                    redirect()
                    setSnackbarMessage('You have successfully logged in');
                    handleClickForSnackbar();
                    
                }

            })
            .catch((error) => setError(error));
    }

    const userRequest = Object.keys(user)
        .reduce((acc, elementKey) => {
            return {
                ...acc,
                [elementKey]: user[elementKey].value
            }
        }, {});

    const handleSignInClick = () => {

        const isValid = Object.keys(user)
            .every(key => user[key].validate(user[key].value));

        if (isValid) {
            login(userRequest);
        } else {
            setInvalidData(true)
        }

    }

    return (
        <>
        <CustomizedSnackbar
                handleCloseSnackbar={handleCloseSnackbar}
                openSnackbar={openSnackbar}
                snackbarMessage={snackbarMessage} />
            
            <LoginForm
                user={user}
                handleChangeForm={handleChangeForm}
                handleSignInClick={handleSignInClick}
                invalidData={invalidData}
                error={error}
            />
        </>
    )

}

export default withRouter(Login);

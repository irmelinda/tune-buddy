import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import constants from '../../common/constants';
import { withRouter } from 'react-router';
import SelectRouteForm from '../../components/Generator/SelectRouteForm';
import { useState } from 'react';
import GeneratedPlaylist from '../../components/Generator/GeneratedPlaylist';
import SelectGenres from '../../components/Generator/SelectGenres';
import { Container, CssBaseline } from '@material-ui/core';
import SelectArtistsRepeat from '../../components/Generator/SelectArtistsRepeat';
import PersonalizedPlaylist from '../../components/Generator/PersonalizePlaylist';
import PlaylistsByDuration from '../../components/Playlists/PlaylistsByDuration';
import { useContext } from 'react';
import AuthContext from '../../providers/authContext';
import { date, month, year, hour, min, sec } from '../../common/constants'
import CustomizedSnackbar from '../../components/Snackbar/Snackbar';
import tracksContext from '../../providers/tracksContext';
import generateTracks from '../../hooks/generateTracks';
import writeDBPlaylistWithTracks from '../../hooks/writeDBPlaylistWithTracks';

const useStyles = makeStyles((theme) => ({
  title: {
    padding: theme.spacing(4, 0, 2),
    color: 'black'
},
  root: {
    width: '100%',
    color: 'black',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white',
    '&:hover': {
      backgroundColor: 'white',
      color: 'black',
    },
    disabledButton: {
      backgroundColor: theme.palette.primary || 'white'
    }
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

const getSteps = () => {
  return [
    'SELECT DESTINATIONS',
    'PERSONALIZE ',
    'CUSTOMIZE PREFERENCES',
    'PREVIEW'
  ];
}

const getStepContent = (
  step,
  props,
  tripDuration,
  repeatArtists,
  setRepeatArtists,
  valueSlider,
  setValueSlider,
  handleReset,
  toggleShowGeneratedPlaylist,
  playlistName,
  handlePlaylistNameForm,
  cover,
  setCover,
  ifError,
  setIfError,
  selectedFile,
  setSelectedFile,
  setMessage,
  setError,
  setTripDuration,
  valueStart,
  setValueStart,
  valueEnd,
  setValueEnd,
  genres,
  setSnackbarMessage,
  handleClickForSnackbar
) => {
  switch (step) {
    case 0:
      return <>
        <SelectRouteForm
          setIfError={setIfError}
          setTripDuration={setTripDuration}
          tripDuration={tripDuration}
          valueStart={valueStart}
          setValueStart={setValueStart}
          valueEnd={valueEnd}
          setValueEnd={setValueEnd}
        />
        {!ifError && <PlaylistsByDuration
          props={props}
          tripDuration={tripDuration}
        />
        }
      </>
    case 1:
      return <PersonalizedPlaylist
        selectedFile={selectedFile}
        setSelectedFile={setSelectedFile}
        playlistName={playlistName}
        handlePlaylistNameForm={handlePlaylistNameForm}
        cover={cover}
        setCover={setCover}
      />
    case 2:
      return <>
        <SelectGenres valueSlider={valueSlider} setValueSlider={setValueSlider} genres={genres} />
        <SelectArtistsRepeat repeatArtists={repeatArtists} setRepeatArtists={setRepeatArtists} />
      </>
    case 3:
      return <GeneratedPlaylist
        valueSlider={valueSlider}
        tripDuration={tripDuration}
        repeatArtists={repeatArtists}
        setMessage={setMessage}
        setError={setError}
        handleReset={handleReset}
        toggleShowGeneratedPlaylist={toggleShowGeneratedPlaylist}
        cover={cover}
        playlistName={playlistName}
        setSnackbarMessage={setSnackbarMessage}
        handleClickForSnackbar={handleClickForSnackbar}

      />
    default:
      return <>
        UNKNOWN STEP
      </>;
  }
}

const VerticalLinearStepper = (props) => {
  const [genres, setGenres] = useState([]);
  const [valueStart, setValueStart] = useState('');
  const [valueEnd, setValueEnd] = useState('');
  const [newPlaylistId, setNewPlaylistId] = useState(null)
  const { tracksValue, setTracksValue } = useContext(tracksContext);
  const authUser = useContext(AuthContext);
  const [ifError, setIfError] = useState(true);
  // SelectArtistsRepeat
  const [repeatArtists, setRepeatArtists] = useState(true);
  // SelectGenres
  const [valueSlider, setValueSlider] = useState([
    {
      genres_id: 85,
      percent: 0
    },
    {
      genres_id: 106,
      percent: 0
    },
    {
      genres_id: 116,
      percent: 0
    },
    {
      genres_id: 129,
      percent: 0
    },
    {
      genres_id: 152,
      percent: 0
    },
    {
      genres_id: 169,
      percent: 0
    },
  ]);
  // Customize/PersonalizePlaylist
  const [playlistName, setPlaylistName] = useState({
    value: `${authUser.user.username}-${date}/${month}/${year}-${hour}:${min}:${sec}`,
    valid: true,
    validate: value => value && value.length > constants.nameMinLength,
    errorText: `Name should be at least 3 characters.`,
  });
  const [selectedFile, setSelectedFile] = useState(null);
  const [cover, setCover] = useState(null)
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();
  const [showGeneratedPlaylist, toggleShowGeneratedPlaylist] = useState(false);
  //trip duration in seconds
  const [tripDuration, setTripDuration] = useState(null);
  const [error, setError] = useState('');
  const [message, setMessage] = useState('');

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  //handle state snackbar
  const handleClickForSnackbar = () => {
    setOpenSnackbar(true);
  };
  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handlePlaylistNameForm = event => {
    playlistName.value = event.target.value;
    playlistName.valid = playlistName.validate(event.target.value);
    playlistName.touched = true;
    setPlaylistName({ ...playlistName });

    playlistName.valid && setIfError(false);
    !playlistName.valid && setIfError(true);

    !playlistName.valid && setSnackbarMessage(playlistName.errorText);
    !playlistName.valid && handleClickForSnackbar();
  };

  const handleNext = () => {
    if (activeStep === 2) {
      generateTracks(valueSlider, tripDuration, repeatArtists, setMessage, setError, setTracksValue, tracksValue);
    }
    if (activeStep === 3) {
      writeDBPlaylistWithTracks(playlistName, cover, setError, setNewPlaylistId, setMessage, props);
      window.location.reload()
    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  useEffect(() => {
    const token = localStorage.token;
    fetch(`https://api.unsplash.com/photos/random?client_id=CXFSdcaBUF5HbvtaQ6xPSDKrg0DOly4kXqt8ViIjRuA`, {
      headers: {
        authorization: `Bearer ${token}`
      },
    })
      .then(response => response.json())
      .then(({ urls }) => {
        if (urls) {
          setCover(urls.full);
        }
      })
      .catch(error => setError(error));

    fetch(`${constants.BASE_URL}/genres`, {
      headers: {
        authorization: `Bearer ${token}`
      },
    })
      .then(response => response.json())
      .then(({ data }) => {
        if (data) {
          setGenres(data);
        }
      })
      .catch(error => setError(error))
  }, []);

  return (
    <>
      <CustomizedSnackbar handleCloseSnackbar={handleCloseSnackbar} openSnackbar={openSnackbar} snackbarMessage={snackbarMessage} />
      <Container maxWidth="lg" className={classes.custom}>
        <Typography component="div" variant="h6" className={classes.title}>
          GENERATOR
        </Typography>

        <CssBaseline />
        <div className={classes.root}>
          <Stepper activeStep={activeStep} orientation="vertical">
            {steps.map((label, index) => (
              <Step key={label} styles={{ color: 'pink' }}>
                <StepLabel>{label}</StepLabel>
                <StepContent>
                  <Typography>
                    {getStepContent(
                      index,
                      props,
                      tripDuration,
                      repeatArtists,
                      setRepeatArtists,
                      valueSlider,
                      setValueSlider,
                      handleReset,
                      toggleShowGeneratedPlaylist,
                      playlistName,
                      handlePlaylistNameForm,
                      cover,
                      setCover,
                      ifError,
                      setIfError,
                      selectedFile,
                      setSelectedFile,
                      setMessage,
                      setError,
                      setTripDuration,
                      valueStart,
                      setValueStart,
                      valueEnd,
                      setValueEnd,
                      genres,
                      setSnackbarMessage,
                      handleClickForSnackbar
                    )
                    }
                  </Typography>

                  <div className={classes.actionsContainer}>
                    <div>
                      {activeStep !== 0
                        ?
                        <Button
                          onClick={handleBack}
                          color="primary"
                          variant="contained"
                          className={classes.button}
                        >
                          Back
                          </Button>
                        : null}
                      < Button
                        disabled={ifError}
                        variant="contained"
                        onClick={activeStep === steps.length ? toggleShowGeneratedPlaylist(true) : handleNext}
                        className={classes.button}
                      >
                        {activeStep === steps.length - 1 ? 'START OVER' : 'NEXT'}
                      </Button>
                    </div>
                  </div>

                </StepContent>
              </Step>
            ))}
          </Stepper>
        </div>

      </Container>
    </>
  );
}
export default withRouter(VerticalLinearStepper);

import { useContext } from 'react';
import './Home.css';
import AuthContext from '../../providers/authContext';
import Button from '@material-ui/core/Button';
import useHttp from '../../hooks/useHttp';
import constants from '../../common/constants';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Typography } from '@material-ui/core';
import PlaylistsHome from '../../components/Playlists/PlaylistsHome';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {        
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2),
        margin: theme.spacing(2, 0, 2),
        borderRadius: '50px',
        backgroundColor: 'black',
        color: 'white',
        '&:hover': {
            backgroundColor: 'white',
            color: 'black',
        },
    },
    title: {
        padding: theme.spacing(4, 0, 2),
        color: 'black'
    }
}));

const Home = (props) => {
    const classes = useStyles();
    const { isLoggedIn } = useContext(AuthContext);

    const { data, loading, message, error } = useHttp(
        `${constants.BASE_URL}/public?playlistsCount=4`, []
    );

    if (loading) {
        return <>Loading...</>;
    }

    if (message) {
        return <>Message...</>;
    }

    if (error) {
        return <>Error...</>;
    }

    return (
        <>
            <div className="home-wallpaper">
                <div className="right">
                    <h3>Create, customize and get the perfect playlist for your ride.</h3>
                    <h1 id="title">SOUNDTRACK YOU RIDE</h1>
                    {!isLoggedIn
                        ?
                        <>
                            <Button variant="contained" className={classes.button} onClick={() => props.history.push(`/create-account`)}>CREATE ACCOUNT</Button>
                            <span> or </span>
                            <Button variant="contained" className={classes.button} onClick={() => props.history.push(`/login`)}>LOG IN</Button>
                        </>
                        : null
                    }
                </div >
            </div>
            <div className={classes.root}>
                <Container component="main" maxWidth="lg">
                    <CssBaseline />
                    
                    <div className={classes.paper}>

                        <Typography component="div" variant="h6" className={classes.title}>
                            POPULAR PLAYLISTS
                        </Typography>

                        <PlaylistsHome props={props} data={data} />

                    </div>
                </Container>
            </div>
        </>
    )
};
export default Home;

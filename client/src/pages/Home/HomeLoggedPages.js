import Button from '@material-ui/core/Button';
import useHttp from '../../hooks/useHttp';
import Playlists from '../../components/Playlists/Playlists';
import constants from '../../common/constants';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Typography } from '@material-ui/core';
import { withRouter } from 'react-router';
import Genres from '../../components/Genres/Genres'
import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/authContext';
import { Pagination } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2),
        margin: theme.spacing(2, 0, 2),
        borderRadius: '50px',
        backgroundColor: 'orange',
        color: 'black',
        '&:hover': {
            backgroundColor: 'black',
            color: 'orange',
        },
    },
    title: {
        padding: theme.spacing(4, 0, 2),
        color: 'black'
    },
    row: {
        direction: 'row'
    },
    pagination:{
        padding: theme.spacing(4,0),  
    }
}));

const Home = (props) => {
    const authUser = useContext(AuthContext);
    const classes = useStyles();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null)
    const [message, setMessage] = useState(null);

    const [page, setPage] = useState(1);
    const handleChange = (event, value) => {
        setPage(value);        
    };
    console.log(data.filter(el=> el.is_deleted!== 1).slice(((page-1)*6), (((page-1)*6)+6)))
    useEffect(() => {
        setLoading(true);
        const token = localStorage.token;
        let mounted = true;

        fetch(`${constants.BASE_URL}/playlists?playlistsCount=500`, {
            headers: {
                authorization: `Bearer ${token}`
            },
        })
            .then(response => response.json())
            .then(({ data:dataP, message }) => {

                if (mounted) {
                    if (!dataP) {
                        setMessage(message);
                    }
                    setData(dataP);
                }
            })
            .catch(error => setError('error'))
            .finally(() => setLoading(false))

        const cleanup = () => {
            mounted = false;
        }

        return cleanup;
    }, []);

    const { data: dataMyPlaylists, loadingMyPlaylists, messageMyPlaylists, errorMyPlaylists } = useHttp(
        `${constants.BASE_URL}/playlists?playlistsCount=18&orderBy=created_on&users_id=${authUser.user.sub}`, []
    );

    const { data: dataGenres, loadingGenres, messageGenres, errorGenres } = useHttp(
        `${constants.BASE_URL}/genres`, []
    );

    if (loading || loadingMyPlaylists || loadingGenres) {
        return <>Loading...</>;
    }

    if (message || messageMyPlaylists || messageGenres) {
        return <>Message...</>;
    }

    if (error || errorMyPlaylists || errorGenres) {
        return <>Error...</>;
    }

    return (
        <div className={classes.root}>
            <Container component="main" maxWidth="lg" className={classes.row}>
                <CssBaseline />
                <div className={classes.paper}>

                    {dataMyPlaylists && dataMyPlaylists.filter(p => p.is_deleted !== 1).length > 0
                        ? <>
                            <Typography component="div" variant="h6" className={classes.title}>
                                MY PLAYLISTS
                            <Button
                                    variant="contained"
                                    className={classes.button}
                                    onClick={() => props.history.push(`/my-account/my-playlists`)}>EXPLORE MORE</Button>
                            </Typography>
                            <Playlists props={props} data={dataMyPlaylists} />

                        </>
                        :
                        <>
                            <Typography component="div" variant="h6" className={classes.title}>
                                NO CUSTOM PLAYLISTS
                            <Button
                                    variant="contained"
                                    className={classes.button}
                                    onClick={() => props.history.push(`/generator`)}>GENERATE FIRST</Button>
                            </Typography>
                            <Playlists props={props} data={dataMyPlaylists} />
                        </>

                    }

                    <Typography component="div" variant="h6" className={classes.title}>
                        FEATURED GENRES
                    </Typography>
                    <Genres data={dataGenres} />

                    <Typography component="div" variant="h6" className={classes.title}>
                        ALL PLAYLISTS
                    </Typography>

                    <Playlists props={props} data={data.filter(el=> el.is_deleted!== 1).slice(((page-1)*6), (((page-1)*6)+6))} />

                    <div className={classes.root}>
                        <Pagination count={Math.ceil(data.filter(el=> el.is_deleted!== 1).length/6)} page={page} onChange={handleChange} className={classes.pagination}/>
                    </div>

                </div>
            </Container>
        </div>
    )
};

export default withRouter(Home);

import { useState } from "react";
import { withRouter } from "react-router";
import constants from "../../common/constants";
import RegisterForm from "./RegisterForm";
import CustomizedSnackbar from '../../components/Snackbar/Snackbar';

const Register = (props) => {
    const [invalidData, setInvalidData] = useState(false);
    const [error, setError] = useState('');
    const [user, setUser] = useState({
        name: {
            value: '',
            valid: true,
            validate: (value) => value && value.length > constants.nameMinLength && value.length < constants.nameMaxLength,
            errorText: `Name length must be between ${constants.nameMinLength} and ${constants.nameMaxLength}`
        },
        username: {
            value: '',
            valid: true,
            validate: (value) => value && value.length > constants.nameMinLength && value.length < constants.nameMaxLength,
            errorText: `Username length must be between ${constants.nameMinLength} and ${constants.nameMaxLength}`
        },
        password: {
            value: '',
            valid: true,
            validate: (value) => value !== undefined && value.length > constants.userPasswordMinLength && constants.passwordRegex.test(value),
            errorText: `Password should contain letters and at least 1 digit`
        },
        // confirmPassword: {
        //     value: '',
        //     valid: true,
        //     validate: (value) => value !== undefined && value !== user.password.value,
        //     errorText: `Password should contain letters and at least 1 digit`
        // },
        email: {
            value: '',
            valid: true,
            validate: (value) => value !== undefined && constants.emailRegex.test(value),
            errorText: `Invalid email format`
        },
    });

    //snackbar state
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [openSnackbar, setOpenSnackbar] = useState(false);

    //handle state snackbar
    const handleClickForSnackbar = () => {
        setOpenSnackbar(true);
    };
    const handleCloseSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnackbar(false);
    };

    const handleChangeForm = name => event => {
        user[name].value = event.target.value;
        user[name].valid = user[name].validate(event.target.value);
        user[name].touched = true;
        setUser({ ...user });
    };

    const redirect = () => setTimeout(() => {
        props.history.push('/login');
    }, 800);

    const register = (userRequest) => {
        fetch(`${constants.BASE_URL}/users/register`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(userRequest)
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message)
                } else {
                    redirect()
                    setSnackbarMessage('You have successfully registered');
                    handleClickForSnackbar();
                }
            })
            .catch((error) => setError(error));
    }

    const userRequest = Object.keys(user)
        .reduce((acc, elementKey) => {

            return {
                ...acc,
                [elementKey]: user[elementKey].value
            }
        }, {});

    const handleRegisterClick = () => {

        const isValid = Object.keys(user)
            .every(key => user[key].validate(user[key].value));

        if (isValid) {
            //handleOpen();
            // setSnackbarMessage("Your account has been successfully created! ")
            // handleClickForSnackbar();
            register(userRequest);

        } else {
            setInvalidData(true)
        }
    }

    return (
        <>
            <CustomizedSnackbar
                handleCloseSnackbar={handleCloseSnackbar}
                openSnackbar={openSnackbar}
                snackbarMessage={snackbarMessage} />

            <RegisterForm
                user={user}
                handleChangeForm={handleChangeForm}
                handleRegisterClick={handleRegisterClick}
                error={error}
            />
        </>
    )

}

export default withRouter(Register);

import React from 'react';
//import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
//import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
//import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="home">
        Tune Buddy
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  // avatar: {
  //   margin: theme.spacing(1),
  //   backgroundColor: theme.palette.secondary.main,
  // },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(3, 0, 3),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white',
    '&:hover': {
        backgroundColor: 'white',
        color: 'black',
    },
},
}));

const RegisterForm = (props) => {
  const { user, handleChangeForm, handleRegisterClick, error } = props;
  const classes = useStyles();

  const onSubmit = e => {
    e.preventDefault();
    handleRegisterClick();
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        {/* <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar> */}
        <Typography component="h1" variant="h5">
          CREATE ACCOUNT
        </Typography>
        {error ? <span className={classes.error} >{error.message}</span> : null}
        <form className={classes.form} noValidate onSubmit={e => onSubmit(e)}>
        <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Full Name"
            name="name"
            autoComplete="name"
            autoFocus
            value={user.name.value}
            onChange={handleChangeForm('name')}
            //error={user.username.ErrorText}
            error={!user.name.valid}
            helperText={user.name.valid ? null : <span>{user.name.errorText}</span>}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            //autoFocus
            value={user.username.value}
            onChange={handleChangeForm('username')}
            //error={user.username.ErrorText}
            error={!user.username.valid}
            helperText={user.username.valid ? null : <span>{user.username.errorText}</span>}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            //autoFocus
            value={user.email.value}
            onChange={handleChangeForm('email')}
            //error={user.username.ErrorText}
            error={!user.email.valid}
            helperText={user.email.valid ? null : <span>{user.email.errorText}</span>}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={user.password.value}
            onChange={handleChangeForm('password')}
            error={!user.password.valid}
            helperText={user.password.valid ? null : <span>{user.password.errorText}</span>}
          />
          {/* <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="confirmPassword"
            label="Confirm Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={user.confirmPassword.value}
            onChange={handleChangeForm('confirmPassword')}
            error={!user.confirmPassword.valid}
            helperText={user.confirmPassword.valid ? null : <span>{user.confirmPassword.errorText}</span>}
          /> */}
          {/* <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          /> */}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
          >
            REGISTER
          </Button>
          <Grid container>
            {/* <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid> */}
            <Grid item>
              <Link href="login" variant="body2">
                {"Already have an account? Sign in"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

export default RegisterForm;

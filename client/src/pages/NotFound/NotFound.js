import './NotFound.css';

const NotFound = () => {

    return (
        <div className="not-found">
        NOT FOUND
        </div>
    )
};

export default NotFound;

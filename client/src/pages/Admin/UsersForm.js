import { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button, CssBaseline, FormControl, Grid, NativeSelect, Tooltip } from "@material-ui/core";
import PersonIcon from '@material-ui/icons/Person';
import DeleteIcon from '@material-ui/icons/Delete';
import AuthContext from '../../providers/authContext';



const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    verticalAlign: 'middle'
  },
  table: {
    minWidth: 650,
  },
  button: {
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white'
  },
  header: {
    paddingTop: 20,
    paddingLeft: 15,

  },
  nav: {

  },
  submitOutlined: {
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    color: 'black'
  },
  red: {
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    color: 'black',
    backgroundColor: 'orange',
    border: 'none'
  },
  paper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  colorSecondary: {
    color: 'orange'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  deleted: {
    color: 'red',
  }
}));

//const roles

const UsersForm = (props) => {
  const classes = useStyles();
  const authUser = useContext(AuthContext);
  const { users, deleteUser, updateRole } = props;

  const handleChange = (event, id) => {

    event.stopPropagation();


    if (event.target.value === 'admin') {
      updateRole(id, '1')
    }
    if (event.target.value === 'member') {
      updateRole(id, '2')
    }

  };

  return (
    <>
      <Grid container component="main">
        <CssBaseline />

        <Grid item xs={12} sm={12} md={12} component={Paper} square >
          <div className={classes.paper}>
            <TableContainer>
              <div style={{ width: '100%' }}>
                <Table className={classes.table} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>ID</TableCell>
                      <TableCell align="left">User Profile</TableCell>
                      <TableCell align="left">Username</TableCell>
                      <TableCell align="left">Full Name</TableCell>
                      <TableCell align="left">Email</TableCell>

                      <TableCell align="left">Status</TableCell>
                      <TableCell align="left">Role</TableCell>
                      <TableCell align="left">Delete User</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {users.length > 0 && users.map((row, i) => (
                      <TableRow key={i}>
                        <TableCell component="th" scope="row">
                          {row.id}
                        </TableCell>
                        <TableCell align="left">
                          <Tooltip title="User profile" aria-label="user-profile">
                            <Button >
                              <PersonIcon />

                            </Button>
                          </Tooltip>
                        </TableCell>
                        <TableCell align="left">{row.username}</TableCell>
                        <TableCell align="left">{row.name}</TableCell>
                        <TableCell align="left">{row.email}</TableCell>
                        {row.is_deleted === 1?
                          <TableCell className={classes.deleted} align="left">deleted</TableCell>
                          : <TableCell align="left">active</TableCell> 
                        }
                        {row.is_deleted === 1 || row.id === authUser.user.sub?
                          <TableCell align="left">{row.role}</TableCell>
                          :
                          <TableCell align="left">
                            <FormControl className={classes.formControl}>
                              <NativeSelect
                                variant='filled'
                                defaultValue={row.role}
                                inputProps={{
                                  id: 'uncontrolled-native',
                                }}
                                onChange={(event) => handleChange(event, row.id)}
                              >
                                <option value={'member'}>member</option>
                                <option value={'admin'}>admin</option>
                              </NativeSelect>
                            </FormControl>
                          </TableCell>
                        }
   
                        <TableCell align="left">
                          <Tooltip title="Delete User" aria-label="user-profile">
                            <Button
                              onClick={() => deleteUser(row.id)}
                              disabled={row.is_deleted === 1 || row.id === authUser.user.sub}>
                              <DeleteIcon />
                            </Button>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </TableContainer>
          </div>
        </Grid>
      </Grid >
    </>
  )
}
export default UsersForm;

import { useState, useEffect } from 'react';
import constants from '../../common/constants';
import useHttp from '../../hooks/useHttp';
import './Admin.css';
import UsersForm from './UsersForm';
import CustomizedSnackbar from '../../components/Snackbar/Snackbar';

const Users = () => {
  const token = localStorage.getItem('token');
  const [users, setUsers] = useState([]);
  const [fetchError, setFetchError] = useState('');

  const { data, loading, message, error } = useHttp(
    `${constants.BASE_URL}/admin/users?orderBy=id&direction=asc`, []
  );

  //snackbar state
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [openSnackbar, setOpenSnackbar] = useState(false);

  //handle state snackbar
  const handleClickForSnackbar = () => {
    setOpenSnackbar(true);
  };
  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };
  useEffect(() => setUsers(data), [data])

  if (loading) {
    return <>Loading...</>;
  }

  if (message) {
    return <>Message...</>;
  }

  if (error) {
    return <>Error...</>;
  };


  const deleteUser = (id) => {

    fetch(`${constants.BASE_URL}/admin/users/${id}`, {
      method: "DELETE",
      mode: "cors",
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUsers(users.map(u => u.id === id ? { ...u, is_deleted: result.data.is_deleted } : { ...u }))
        setSnackbarMessage('User successfully deleted');
        handleClickForSnackbar();


      })
      .catch((error) => setFetchError(error))
  }



  const updateRole = (id, role) => {

    fetch(`${constants.BASE_URL}/admin/users/${id}/update`, {
      method: "PUT",
      mode: "cors",
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        role: role,
      })
    })
      .then((response) => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }
        // const copy = [...users];
        //   const index = users.findIndex(u => u.id === id);
        //   copy[index] = { ...copy[index], ...result.data.role };
        //   setUsers(copy);

        setUsers(users.map(u => u.id === id ? { ...u, role: result.data.role } : { ...u }))
        setSnackbarMessage('Role successfully updated');
        handleClickForSnackbar();


      })
      .catch((error) => setFetchError(error))
  }

  return (
    <>
      <CustomizedSnackbar
        handleCloseSnackbar={handleCloseSnackbar}
        openSnackbar={openSnackbar}
        snackbarMessage={snackbarMessage} />
      <UsersForm
        users={users}
        deleteUser={deleteUser}
        updateRole={updateRole}
      />

    </>
  )
};

export default Users;

import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button, CssBaseline, FormControl, Grid, InputLabel, NativeSelect, Select, Tooltip, Typography } from "@material-ui/core";
import PersonIcon from '@material-ui/icons/Person';
import EditIcon from '@material-ui/icons/Edit';
import BlockIcon from '@material-ui/icons/Block';
import DeleteIcon from '@material-ui/icons/Delete';
import MusicNoteIcon from '@material-ui/icons/MusicNote';



const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    verticalAlign: 'middle'
  },
  table: {
    minWidth: 650,
  },
  button: {
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    backgroundColor: 'black',
    color: 'white'
  },
  header: {
    paddingTop: 20,
    paddingLeft: 15,

  },
  nav: {

  },
  submitOutlined: {
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    color: 'black'
  },
  red: {
    margin: theme.spacing(2, 0, 2),
    borderRadius: '50px',
    color: 'black',
    backgroundColor: 'orange',
    border: 'none'
  },
  paper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  colorSecondary: {
    color: 'orange'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  deleted: {
    color: 'red',
  }
}));

//const roles

const PlaylistsForm = (props) => {
  const classes = useStyles();
  const { playlists } = props;

  //return JSON.stringify(users);
  const [state, setState] = useState({});

  return (
    <>
      <Grid container component="main">
        <CssBaseline />

        <Grid item xs={12} sm={12} md={12} component={Paper} square >
          <div className={classes.paper}>

            <TableContainer>
              <div style={{ width: '100%' }}>
                <Table className={classes.table} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>ID</TableCell>
                      <TableCell align="left">Preview</TableCell>
                      <TableCell align="left">Name</TableCell>
                      <TableCell align="left">Duration</TableCell>
                      <TableCell align="left">Tracks Count</TableCell>
                      <TableCell align="left">Avg Rank</TableCell>
                      <TableCell align="left">User ID</TableCell>
                      <TableCell align="left">Status</TableCell>
                      <TableCell align="left">Delete</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {playlists.length > 0 && playlists.map((row, i) => (
                      <TableRow key={i}>
                        <TableCell component="th" scope="row">
                          {row.id}
                        </TableCell>
                        <TableCell align="left">
                          <Tooltip title="User profile" aria-label="user-profile">
                            <Button >
                              <MusicNoteIcon />

                            </Button>
                          </Tooltip>
                        </TableCell>
                        <TableCell align="left">{row.name}</TableCell>
                        <TableCell align="left">{row.duration}</TableCell>
                        <TableCell align="left">{row.tracks_count}</TableCell>
                        <TableCell align="left">{Math.floor(row.tracks_avg_rank)}</TableCell>
                        <TableCell align="left">{row.users_id}</TableCell>
                        {row.is_deleted === 1?
                          <TableCell className={classes.deleted} align="left">deleted</TableCell>
                          : <TableCell align="left">active</TableCell> 
                        }

                        <TableCell align="left">
                          <Tooltip title="Delete User" aria-label="user-profile">
                            <Button
                              // onClick={() => deleteUser(row.id)}
                              disabled={row.is_deleted === 1}>
                              <DeleteIcon />
                            </Button>
                          </Tooltip>
                        </TableCell>

                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </TableContainer>
          </div>
        </Grid>
      </Grid >
    </>
  )
}
export default PlaylistsForm;

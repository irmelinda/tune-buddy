import { useEffect, useState } from 'react';
import constants from '../../common/constants';
import useHttp from '../../hooks/useHttp';
import './Admin.css';
import PlaylistsForm from './PlaylistsForm';

const Playlists = () => {
    const [playlists, setPlaylists] = useState([]);


    const { data, loading, message, error } = useHttp(
        `${constants.BASE_URL}/playlists?orderBy=id&direction=asc&playlistsCount=500`, []
    );

    useEffect(() => setPlaylists(data),[data])

    if (loading) {
        return <>Loading...</>;
    }

    if (message) {
        return <>Message...</>;
    }

    if (error) {
        return <>Error...</>;
    };

    return (
        <PlaylistsForm
        playlists={playlists}
    />
    )
};


export default Playlists;

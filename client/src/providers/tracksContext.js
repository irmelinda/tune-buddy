import { createContext } from 'react';

const tracksContext = createContext(null);

export default tracksContext;

import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  setAuthState: () => {},
});

export const getUser = () => {
  try {
    return jwtDecode(localStorage.getItem('token') || '');
  } catch (error) {
    return null;
  }
};

export default AuthContext;

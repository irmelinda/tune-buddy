import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import { PORT } from '../config.js';
import adminController from './controllers/admin.controller.js';
import usersController from './controllers/users.controller.js';
import versionMiddleware from './middlewares/global/version.middleware.js';
// import loggerMiddleware from './middlewares/global/logger.middleware.js';
import errorMiddleware from './middlewares/global/error.middleware.js';
import playlistsController from './controllers/playlists.controller.js';
import publicController from './controllers/public.controller.js';
import genresController from './controllers/genres.controller.js';


passport.use(jwtStrategy);

// create an instance of the server
const app = express();

// global middlewares 
app.use(express.json());
app.use(cors());
app.use(helmet());
app.use(passport.initialize());

// global middlewares - custom
app.use(versionMiddleware);
// app.use(loggerMiddleware);

app.use('/avatars', express.static('avatars'));
app.use('/playlists_covers', express.static('playlists_covers'));

app.use('/admin', adminController);
app.use('/users', usersController);
app.use('/playlists', playlistsController);
app.use('/public', publicController);
app.use('/genres', genresController);

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found' }),
);

app.use(errorMiddleware);

app.listen(PORT, () => console.log(`Listening on port ${PORT}...`));
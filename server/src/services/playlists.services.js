import serviceErrors from '../common/service-errors.js';
import serviceMessages from '../common/service-messages.js';

const playlistsServices = {

  getPlaylistsGenres: genresData => {
    return async () => {
      const data = await genresData.getAll();

      if (!data[0]) {

        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      return { error: null, data, message: serviceMessages.RETRIEVED };
    };

  },

  getPlaylists: playlistsData => {

    return async (reqQuery, user) => {
      // eslint-disable-next-line no-unused-vars
      const { orderBy, direction, page, playlistsCount, ...rest } = reqQuery;

      const restArray = Object.keys(rest);

      const all = async (reqQuery) => {
        const data = await playlistsData.getAll(reqQuery, user);
        if (!data[0]) {

          return {
            error: serviceErrors.RECORD_NOT_FOUND,
            data: null,
            message: serviceMessages.DOES_NOT_EXIST,
          };
        }

        return { error: null, data, message: serviceMessages.RETRIEVED };
      };

      const BySearchCriteria = async (reqQuery) => {
        const data = await playlistsData.getBySearchCriteria(reqQuery, user);
        if (!data[0]) {

          return {
            error: serviceErrors.RECORD_NOT_FOUND,
            data: null,
            message: serviceMessages.DOES_NOT_EXIST,
          };
        }

        return { error: null, data, message: serviceMessages.RETRIEVED };
      };

      return restArray.length === 0
        ? await all(reqQuery)
        : await BySearchCriteria(reqQuery);
    };
  },

  getPlaylistsBySearchCriteria: playlistsData => {

    return async (column, value, reqQuery) => {
      const data = await playlistsData.getBy(column, value, reqQuery);

      if (!data[0]) {

        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      return { error: null, data, message: serviceMessages.RETRIEVED };
    };
  },

  getPlaylistById: playlistsData => {

    return async (id, user) => {
      const data = await playlistsData.getBy('id', id, user);

      if (!data[0]) {

        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      return { error: null, data, message: serviceMessages.RETRIEVED };
    };
  },

  createPlaylist: (playlistsData, playlistsTracksData) => {

    return async (playlistToCreate, userId) => {
      // const res = await playlistsData.getBy('name', playlistToCreate.name);
      // if (res[0]) {

      //   return {
      //     error: serviceErrors.OPERATION_NOT_PERMITTED,
      //     data: null,
      //     message: serviceMessages.EXISTS,
      //   };
      // }
      const playlistCreate = await playlistsData.create(playlistToCreate, userId);
      const dataPlaylist = await playlistsData.getBySimple('id', playlistCreate.insertId);

      await playlistsTracksData.create(playlistToCreate.tracks_list , playlistCreate.insertId);
      const dataTracks = await playlistsTracksData.getBy('playlists_id', playlistCreate.insertId);

      return { error: null, data: {playlist:dataPlaylist, tracks:dataTracks}, message: serviceMessages.CREATED };
    };
  },

  updatePlaylist: playlistsData => {

    return async (id, playlistToUpdateWith, user) => {
      const playlistToUpdate = await playlistsData.getBy('id', id);
      if (!playlistToUpdate[0]) {

        return {
          error: serviceErrors.UN,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }
      if (user.role !== 'admin') {
        if (playlistToUpdate[0].users_id !== user.id) {

          return {
            error: serviceErrors.OPERATION_NOT_PERMITTED,
            data: null,
            message: serviceMessages.UNAUTHORIZED,
          };
        }
      }
      if (playlistToUpdateWith.name) {
        const playlistToUpdate1 = await playlistsData.getBy('name', playlistToUpdateWith.name);
        if (playlistToUpdate1[0]) {

          return {
            error: serviceErrors.DUPLICATE_RECORD,
            data: null,
            message: serviceMessages.DUPLICATE,
          };
        }
      }
      if (Object.keys(playlistToUpdateWith).every(key => playlistToUpdateWith[key] === playlistToUpdate[0][key])) {

        return {
          error: serviceErrors.DUPLICATE_RECORD,
          data: null,
          message: serviceMessages.DUPLICATE,
        };
      }
      await playlistsData.update(id, playlistToUpdateWith);
      const data = await playlistsData.getBy('id', id);

      return { error: null, data, message: serviceMessages.UPDATED };
    };
  },

  deletePlaylist: playlistsData => {

    return async (id, user) => {

      console.log(id);

      const res = await playlistsData.getBy('id', id);
      if (!res[0]) {

        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }
      await playlistsData.delete(id);
      const data = await playlistsData.getBy('id', id, user);

      return { error: null, data, message: serviceMessages.DELETED };
    };
  },
};

export default playlistsServices;
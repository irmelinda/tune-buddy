//import serviceErrors from '../common/service-errors.js';
//import serviceMessages from '../common/service-messages.js';
import serviceErrors from '../common/service-errors.js';
import { loadLocation } from '../requests/bing-api-requests.js';

const durationServices = {

  getDuration: playlistData => {
    return async (startPoint, endPoint) => {
      const trip = await loadLocation(startPoint, endPoint);
      //get trip duration in seconds
      const tripDuration = trip.resourceSets[0].resources[0].routeLegs[0].travelDuration;
      console.log('log duration');
      console.log(tripDuration);
      const playlists = await playlistData.getByDuration(tripDuration)
      if (playlists.length === 0) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          playlist: null,
          message: 'No playlists with desired criteria found.'
        };
      }
      return {error: null, playlists: playlists, message: 'Success'};
    };
  },

};



export default durationServices;





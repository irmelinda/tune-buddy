import playlistAlgorithm from '../algorithm/algorithm.js';
//import serviceErrors from '../common/service-errors.js';
import serviceMessages from '../common/service-messages.js';

const generatorServices = { 

  generatePlaylist: (tracksData, genresData) => {
    return async (reqBody) => {
      const {duration, repeatArtist, genres} = reqBody;

      const res = await playlistAlgorithm(duration, repeatArtist, genres, tracksData, genresData);

      return { error: null, data: res, message: serviceMessages.GENERATED };
    };
  },
};

export default generatorServices;
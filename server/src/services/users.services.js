import bcrypt from 'bcrypt';
import { MLS_TO_DAYS } from '../../config.js';
import userRoles from '../common/user-roles.js';

import serviceErrors from '../common/service-errors.js';
import serviceMessages from '../common/service-messages.js';

export default {

  getAllUsers: usersData => {
    return async (reqQuery) => {
      const { orderBy, direction, page, usersCount, ...rest } = reqQuery;

      const restArray = Object.keys(rest);

      if (restArray.length === 0) {
        const res = await usersData.getAll(reqQuery);
        if (!res) {
          return {
            error: serviceErrors.RECORD_NOT_FOUND,
            data: null,
            message: serviceMessages.DOES_NOT_EXIST,
          };
        } else {
          return {
            error: null,
            data: res,
            message: serviceMessages.SUCCESS,
          };
        }
      } else {
        const res = await usersData.searchBy(reqQuery);
        if (!res) {
          return {
            error: serviceErrors.RECORD_NOT_FOUND,
            data: null,
            message: serviceMessages.DOES_NOT_EXIST,
          };
        } else {
          return {
            error: null,
            data: res,
            message: serviceMessages.SUCCESS,
          };
        }
      }
    };
  },

  getUserById: usersData => {
    return async (id) => {
      const user = await usersData.getBy('id', id);
      // console.log('log into service');
      // console.log(user);

      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      return { error: null, data: user, message: serviceMessages.SUCCESS };
    };
  },

  createUser: usersData => {
    return async (userCreate) => {
      const { username, password, name, email } = userCreate;

      const existingUser = await usersData.getBy('email', email);
      console.log('log in reg service');
      console.log(existingUser);

      if (existingUser) {//(existingUser && existingUser.is_deleted === 0) {
        return {
          error: serviceErrors.DUPLICATE_RECORD,
          data: null,
          message: serviceMessages.DUPLICATE,
        };
      }

      const passwordHashed = await bcrypt.hash(password, 10);

      const user = await usersData.create(username, passwordHashed, name, email, userRoles.Member);

      return { error: null, data: user, message: serviceMessages.CREATED };
    };
  },

  deleteUser: usersData => {
    return async (id) => {
      const user = await usersData.getBy('id', id);
      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      // if (user.is_deleted === 1) {
      //   return {
      //     error: serviceErrors.RECORD_NOT_FOUND,
      //     data: null,
      //     message: serviceMessages.DOES_NOT_EXIST,
      //   };
      // }
      await usersData.remove(id);
      const user2 = await usersData.getBy('id', id);

      return { error: null, data: user2, message: serviceMessages.DELETED };
    };
  },

  signInUser: usersData => {

    return async (email, password) => {

      const user = await usersData.getWithRole(email);
      console.log('log in services');
      console.log(user);

      if (!user || !(await bcrypt.compare(password, user.password))) {
        return {
          error: serviceErrors.INVALID_SIGN_IN,
          data: null,
          message: serviceMessages.INVALID,
        };
      }

      const banDate = new Date(user.ban_expiration_date);

      if (banDate.valueOf() > Date.now()) {
        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED,
          user: null,
          message: ` ${serviceMessages.BANNED} - ${Math.floor((banDate.valueOf() - Date.now()) / (MLS_TO_DAYS))} day/s`,
        };
      }

      return {
        error: null,
        data: user,
        message: serviceMessages.SUCCESS,
      };
    };
  },

  logoutUser: (usersData) => {
    return async (token) => {
      const logout = await usersData.logout(token);
      if ( logout.length === 0 ) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.INVALID,
        };
      }
      return {
        error: null,
        data: logout,
        message: serviceMessages.SUCCESS,
      };
    };
  },

  banUser: (usersData) => {
    return async (id) => {
      const user = await usersData.getBy('id', id);
      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }
      if (user.ban_expiration_date !== null) {
        return {
          error: serviceErrors.DUPLICATE_RECORD,
          data: null,
          message: serviceMessages.BANNED,
        };
      }
      await usersData.ban(id);
      const userBanned = await usersData.getBy('id', id);
      return {
        error: null,
        data: userBanned,
        message: serviceMessages.BANNED,
      };

    };
  },

  updateUserPassword: (usersData) => {

    return async (reqBody, reqUser) => {
      // console.log('log in services 189');
      // console.log(reqBody);
      // console.log(reqUser);
      // console.log('end of log');
      const { password, newPassword } = reqBody;
      const user = await usersData.getWithRole(reqUser.email);
      // console.log('and the user is:');
      // console.log(user);
      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }
      const match = (await bcrypt.compare(password, user.password));
      // console.log(match);
      if (!match) {
        // console.log('it is in the IF state');
        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED,
          data: null,
          message: serviceMessages.INVALID,
        };
      }

      const passwordHashed = await bcrypt.hash(newPassword, 10);
      // console.log('log after hash:');
      // console.log(reqUser.id);
      await usersData.update('password', passwordHashed, reqUser.id);
      const updatedUser = await usersData.getBy('id', reqUser.id);
      // console.log(updatedUser);

      return { error: null, data: updatedUser, message: serviceMessages.SUCCESS };
    };
  },

  updateUserProfile: usersData => {
    return async (reqBody, id) => {
      const user = await usersData.getBy('id', id);
      // console.log('log in services');
      // console.log(user);
      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }
      if (reqBody.username) {
        const user = await usersData.getBy('username', reqBody.username);
        // console.log('log in if?');
        // console.log(user);
        if (user) {
          return {
            error: serviceErrors.RECORD_NOT_FOUND,
            data: null,
            message: serviceMessages.DUPLICATE,
          };
        }
      }
      if (reqBody.email) {

        const user = await usersData.getBy('email', reqBody.email);
        if (!user) {
          return {
            error: serviceErrors.RECORD_NOT_FOUND,
            data: null,
            message: serviceMessages.DOES_NOT_EXIST,
          };
        }
      }
      Object.keys(reqBody).map(async (key) => {
        // console.log(key);
        // console.log(reqBody[key]);
        await usersData.update(key, reqBody[key], id);
      });
      const updatedUser = await usersData.getBy('id', id);

      return { error: null, data: updatedUser, message: serviceMessages.SUCCESS };
    };
  },

  //accessible only for admins
  updateUserRole: usersData => {
    return async (reqBody, id) => {
      const user = await usersData.getBy('id', id);
      // console.log('log in update role');
      // console.log(user);
      // console.log(reqBody);
      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      await usersData.update('roles_id', +reqBody.role, id);
      const updatedUser = await usersData.getBy('id', id);
      return {
        error: null,
        data: updatedUser,
        message: serviceMessages.SUCCESS,
      };
    };
  },

  updateUserAvatar: usersData => {
    return async (file, id) => {
      const user = await usersData.getBy('id', id);
      // console.log('log in update role');
      // console.log(user);
      // console.log(reqBody);
      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }
      await usersData.update('avatar_url', `http://localhost:5555/avatars/${file}`, id);
      const updatedUser = await usersData.getBy('id', id);
      return {
        error: null,
        data: updatedUser,
        message: serviceMessages.SUCCESS,
      };
    };
  },

  getMyAccount: usersData => {
    return async (reqUser) => {
      const user = await usersData.getBy('id', reqUser.id);
      if (!user) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }
      return {
        error: null,
        data: user,
        message: serviceMessages.SUCCESS,
      };
    };
  },
};











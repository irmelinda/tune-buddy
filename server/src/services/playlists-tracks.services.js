import serviceErrors from '../common/service-errors.js';
import serviceMessages from '../common/service-messages.js';
import playlistsData from '../data/playlists.data.js';
import tracksData from '../data/tracks.data.js';

const playlistsTracksServices = {

  getTracksByPlaylistId: (playlistsData, playlistsTracksData) => {

    return async (id) => {

      const res1 = await playlistsData.getBy('id', id);

      if (!res1[0]) {

        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      const data = await playlistsTracksData.getBy('playlists_id', id);

      if (!data[0]) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      return { error: null, data, message: serviceMessages.RETRIEVED };
    };
  },

  createTracksList: (playlistsData, playlistsTracksData, tracksData) => {

    return async (tracksIdsList, id) => {
      const res1 = await playlistsData.getBy('id', id);
      if (!res1[0]) {

        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      const res2 = await tracksData.getMultipleById(tracksIdsList);
      console.log(res2.length);
      if (res2.length !== tracksIdsList.length) {

        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
          message: serviceMessages.DOES_NOT_EXIST,
        };
      }

      const res3 = await playlistsTracksData.getBy('playlists_id', id);
      if (res3[0]) {

        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED,
          data: null,
          message: serviceMessages.EXISTS,
        };
      }

      await playlistsTracksData.create(tracksIdsList, id);
      const data = await playlistsTracksData.getBy('playlists_id', id);
      return { error: null, data, message: serviceMessages.CREATED };
    };
  },
};

export default playlistsTracksServices;
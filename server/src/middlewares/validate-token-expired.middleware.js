import jwt from 'jsonwebtoken';

export default async (req, res, next) => {
  console.log('log in middleware - check expiration');
  console.log(req.user);
  const token = req.headers.authorization.replace('Bearer ', '');

  const { exp } = jwt.decode(token);
  console.log(exp);
  console.log(Date.now());

  if (Date.now() >= exp * 1000) {
    return res.status(401).send({ message: `Your session is expired.`});
  }

  await next();
};

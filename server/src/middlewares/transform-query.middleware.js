export default (validator) => async (req, res, next) => {
    Object.keys(req.query).forEach(key => {
        if (!validator[key] || req.query[key] === 'undefined') {
            delete req.query[key];

        }
    });

    await next();
};
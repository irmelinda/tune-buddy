import errorMessages from '../common/error-messages.js';

export default async (req, res, next) => {
  const isNotInteger = Object.values(req.params).some(param => isNaN(+param));
  if (isNotInteger) {

    return res.status(400).send({ message: errorMessages.params });
  }

  await next();
};

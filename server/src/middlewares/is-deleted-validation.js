import usersData from '../data/users.data.js';

export default async (req, res, next) => {
  const user = await usersData.getBy('id', req.user.id);
  
  if (!user) {
    return res.status(403).send({ message: `There is no such user!` });
  }

  if (user.is_deleted === 1) {
    return res.status(403).send({ message: `Please, register to access all site features!` });
  }

  await next();
};

import checkToken from '../data/tokens.js';

export default async (req, res, next) => {
  
  const token = req.headers.authorization.replace('Bearer ', '');
  if (await checkToken(token)) {
    return res.status(401).send({ message: `You're not logged in!`});
  }

  await next();
};

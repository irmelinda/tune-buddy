import errorMessages from '../common/error-messages.js';

export default (resourceName, validator) => async (req, res, next) => {
  const errors = {};

  Object
    .keys(validator)
    .forEach(key => {
      if (!validator[key](req.body[key]) || req.body[key] === '' || req.body[key] === null) {
        errors[key] = errorMessages[resourceName][key];
      }
    });

  if (Object.keys(errors).length > 0 ) {

    return res.status(400).send({ message: { errors } });
  }

  await next();
};

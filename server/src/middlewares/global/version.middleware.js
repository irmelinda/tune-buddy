export default (req, res, next) => {

    res.set('Infinity-Book-Club', '0.1.0');
    
    next();
};
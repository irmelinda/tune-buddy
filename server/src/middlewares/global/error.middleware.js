export default async (err, req, res, next) => {

  res.status(500).send({
    message: 'An unexpected error occurred, our developers are working hard to resolve it.',
  });

  await next();
};
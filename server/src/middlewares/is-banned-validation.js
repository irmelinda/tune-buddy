import usersData from '../data/users.data.js';

export default async (req, res, next) => {
  const user = await usersData.getBy('id', req.user.id);

  if (!user) {
    return res.status(403).send({ message: `There is no such user!` });
  }

  if (user.ban_expiration_date) {
    const banDate = new Date(user.ban_expiration_date);

    if (banDate.valueOf() > Date.now()) {
      return res.status(403).send({ message: `You're banned!` });
    }

    await usersData.liftBan(user.id);
  }

  await next();
};

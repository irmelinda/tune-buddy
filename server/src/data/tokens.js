import db from '../../config.js';

const checkToken = async (token) => {

  const result = await db.query(`SELECT * 
                                 FROM tokens t 
                                 WHERE t.token = ?;`, [token]);

  return result && result.length > 0;
};

export default checkToken;
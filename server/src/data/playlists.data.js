import db from '../../config.js';
import { constants } from '../common/constants.js';

const playlistsData = {

  getAll: async (reqQuery, user) => {
    let { orderBy = 'tracks_avg_rank', direction = 'desc', page = 0, playlistsCount = constants.playlistsCountMinValue } = reqQuery;

    if (page < 0) {
      page = 0;
    }
    if (playlistsCount < constants.playlistsCountMinValue) {
      playlistsCount = constants.playlistsCountMaxValue;
    }
    if (playlistsCount > constants.playlistsCountMaxValue) {
      playlistsCount = constants.playlistsCountMaxValue;
    }

    const sql = `
            SELECT 
                p.id,
                p.name,
                p.cover_url,
                SUM(t.duration) AS duration,
                COUNT(pt.playlists_id) AS tracks_count,
                AVG(t.rank) AS tracks_avg_rank,
                p.is_deleted,
                p.users_id,
                p.created_on,
                u.username
            FROM playlists p
                LEFT JOIN playlists_tracks pt ON pt.playlists_id = p.id
                LEFT JOIN tracks t ON t.id = pt.tracks_id
                LEFT JOIN users u ON u.id = p.users_id
                ${user === undefined
        ? 'WHERE p.is_deleted = 0'
        : user.role === 'member'
          ? 'WHERE p.is_deleted = 0'
          : ''}
            GROUP BY p.id
            ORDER BY ${orderBy} ${direction}
            LIMIT ${page * playlistsCount}, ${playlistsCount}`;

    return await db.query(sql);
  },

  getBySearchCriteria: async (reqQuery, user) => {
    let { orderBy = 'tracks_avg_rank', direction = 'desc', page = 0, playlistsCount = constants.playlistsCountMinValue, ...rest } = reqQuery;

    if (page < 0) {
      page = 0;
    }
    if (playlistsCount < constants.playlistsCountMinValue) {
      playlistsCount = constants.playlistsCountMaxValue;
    }
    if (playlistsCount > constants.playlistsCountMaxValue) {
      playlistsCount = constants.playlistsCountMaxValue;
    }

    const sql1 = `
            SELECT 
                p.id,
                p.name,
                p.cover_url,
                SUM(t.duration) AS duration,
                COUNT(pt.playlists_id) AS tracks_count,
                AVG(t.rank) AS tracks_avg_rank,
                p.is_deleted,
                p.users_id,
                p.created_on,
                u.username
            FROM playlists p
                LEFT JOIN playlists_tracks pt ON pt.playlists_id = p.id
                LEFT JOIN tracks t ON t.id = pt.tracks_id
                LEFT JOIN users u ON u.id = p.users_id
         
                ${user === undefined
        ? 'WHERE p.is_deleted = 0'
        : user.role === 'member'
          ? 'WHERE p.is_deleted = 0'
          : ''}
            GROUP BY p.id
            HAVING `;

    const sql2 = Object.keys(rest)
      .map(k => [k, reqQuery[k]])
      .map(([key, value]) => key === 'duration'
        ? `duration BETWEEN (${value} - (5 * 60)) AND (${value} + (5 * 60))`
        : key === 'users_id' ? `${key}=${value}` : `${key} LIKE '%${value}%'`)
      .join(' AND ');

    const sql = sql1 + sql2 + ` ORDER BY ${orderBy} ${direction}
    LIMIT ${page * playlistsCount}, ${playlistsCount}; `;

    return await db.query(sql);
  },

  getBy: async (column, value, user) => {
    const sql = `
      SELECT
        p.id,
        p.name,
        p.cover_url,
        COUNT(pt.playlists_id) AS tracks_count,
          SUM(t.duration) AS duration,
            AVG(t.rank) AS tracks_avg_rank,
              p.is_deleted,
              p.users_id,
              u.username
      FROM playlists p
      LEFT JOIN playlists_tracks pt ON pt.playlists_id = p.id
      LEFT JOIN tracks t ON t.id = pt.tracks_id
      LEFT JOIN users u ON u.id = p.users_id
      ${user === undefined ? 'WHERE p.is_deleted = 0' : ''}
      GROUP BY p.id
      HAVING ${column} = ?;
    `;

    return await db.query(sql, [value]);
  },

  getBySimple: async (column, value, user) => {
    const sql = `
      SELECT
        p.id,
        p.name,
        p.cover_url,
        p.is_deleted
      FROM playlists p
      WHERE p.${column} = ? ${user === undefined ? 'AND p.is_deleted = 0' : ''};
    `;

    return await db.query(sql, [value]);
  },

  create: async (playlistToCreate, user) => {
    const sql = `
      INSERT INTO playlists (users_id, name, cover_url)
      VALUES(?, ?, ?);
    `;

    return await db.query(sql, [user.id, playlistToCreate.name, playlistToCreate.cover_url]);
  },

  update: async (id, playlistToUpdateWith) => {

    const sql1 = `
      UPDATE playlists
      SET `;

    const sql2 = Object.keys(playlistToUpdateWith)
      .map(key => [key, playlistToUpdateWith[key]])
      .map(([key, value]) => `${key} = '${value}'`)
      .join(', ');

    const sql3 = ` WHERE id = ${id}; `;
    const sqlFinal = sql1 + sql2 + sql3;

    return await db.query(sqlFinal, Object.values(playlistToUpdateWith));
  },

  delete: async (id) => {
    const sql = `
      UPDATE playlists
      SET is_deleted = 1
      WHERE id = ?;
    `;

    return await db.query(sql, [id]);
  },

};

export default playlistsData;
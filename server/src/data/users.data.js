import db, { USER_BAN_PERIOD } from '../../config.js';
import { constants } from '../common/constants.js';

export default {

  getAll: async (reqQuery) => {
    let { orderBy = 'id', direction = 'desc', page = 0, usersCount = constants.usersCountMin } = reqQuery;

    if (page < 0) {
      page = 0;
    }
    if (usersCount < constants.usersCountMin) {
      usersCount = constants.usersCountMin;
    }
    if (usersCount > constants.usersCountMax) {
      usersCount = constants.usersCountMax;
    }
    const sql = `
            SELECT u.id, u.username, u.name, u.email, u.ban_expiration_date, u.is_deleted, u.avatar_url, r.name as role       
            FROM users u
            JOIN roles r ON u.roles_id = r.id
            ORDER BY ${orderBy} ${direction}
            LIMIT ${page * usersCount}, ${usersCount}
            `;
    //
    //return await db.query(sql);

    const result = await db.query(sql);
    console.log('log in data');
    console.log(result);
    return result;
  },

  getBy: async (column, value) => {
    const sql = `
            SELECT u.id, u.username, u.name, u.email, u.avatar_url, u.is_deleted, u.ban_expiration_date, r.name as role
            FROM users u 
            JOIN roles r ON u.roles_id = r.id
            WHERE u.${column} = ?
            
        `;

    const result = await db.query(sql, [value]);
    // console.log('log in data');
    // console.log(result);

    return result[0];
  },

  getWithRole: async (email) => {
    const sql = `
            SELECT u.id, u.username, u.password, u.avatar_url, u.email, u.name, u.ban_expiration_date, r.name as role
            FROM users u
            JOIN roles r ON u.roles_id = r.id
            WHERE u.email = ? 
            AND is_deleted = 0 
        `;

    const result = await db.query(sql, [email]);

    return result[0];
  },

  searchBy: async (reqQuery) => {
    const { orderBy = 'username', direction = 'asc', page = 0, usersCount = constants.usersCountMin, ...rest } = reqQuery;
    let sql = `
            SELECT u.id, u.username, u.name, u.email, u.ban_expiration_date, u.is_deleted, u.avatar_url 
            FROM users
            WHERE is_deleted = 0 AND `;
    const sql2 = Object.keys(rest)
      .map(k => [k, reqQuery[k]])
      .map(([key, value]) => `${key} LIKE '%${value}%'`)
      .join(' AND ');

    sql = sql + sql2 + `ORDER BY ${orderBy} ${direction}
    LIMIT ${page * usersCount}, ${usersCount}`;

    return await db.query(sql);
  },

  create: async (username, password, name, email, roleId) => {
    const sql = `
            INSERT INTO users (username, password, name, email, roles_id)
            VALUES (?, ?, ?, ?, ?)
        `;

    const result = await db.query(sql, [username, password, name, email, roleId]);

    return {
      id: result.insertId,
      username: username,
      name: name,
      email: email,
      roleId: roleId,
    };
  },

  update: async (column, value, id) => {
    const sql = `
            UPDATE users 
            SET ${column} = ?
            WHERE id = ?
        `;

    return await db.query(sql, [value, id]);
  },

  remove: async (id) => {
    const sql = `
            UPDATE users 
            SET is_deleted = 1
            WHERE id = ?
        `;

    return await db.query(sql, [id]);
  },

  logout: async (token) => {
    return await db.query(`
            INSERT INTO tokens (token) 
            VALUES (?)`, [token]);
  },

  ban: async (id) => {
    return await db.query(`
            UPDATE users u 
            SET u.ban_expiration_date = ?
            WHERE u.id = ?`, [new Date(Date.now() + USER_BAN_PERIOD), id]);
  },

  lift: async (id) => {
    return await db.query(`
            UPDATE users u 
            SET u.banDate = NULL 
            WHERE u.id = ?`, [id]);
  },
};

import db from '../../config.js';

const genresData = {

  getAll: async () => {
    console.log('zhiri in get all');
    const sql = `
      SELECT g.id, g.name, g.picture_medium_url , count(t.albums_id) as tracks_count
      FROM genres g
      left join artists a on g.id=a.genres_id
      left join albums al on a.id=al.artists_id
      left join tracks t on al.id=t.albums_id 
      group by g.id; 
    `;
    return await db.query(sql);
  },
};

export default genresData;



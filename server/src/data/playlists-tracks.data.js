import db from '../../config.js';

const playlistsTracksData = {

  getBy: async (column, value) => {
    const sql = `
      SELECT 
        pt.playlists_id,
        pt.tracks_id, 
        t.title, 
        t.duration, 
        t.rank, 
        t.albums_id, 
        t.artists_id, 
        ar.name,
        g.id AS genre_id, 
        g.name as genre_name, 
        a.title as album_title
      FROM playlists_tracks pt
      LEFT JOIN tracks t ON pt.tracks_id=t.id
      LEFT JOIN albums a ON a.id = t.albums_id
      LEFT JOIN genres g ON g.id=a.genres_id
      LEFT JOIN artists ar ON ar.id=t.artists_id
      WHERE pt.${column} = ?;
      `;

    return await db.query(sql, [value]);
  },

  getSetOfTracks: async (tracksIdsList) => {
    const sql = `
    ${tracksIdsList.map(el => `(
        SELECT pt.playlists_id 
        FROM playlists_tracks pt 
        WHERE pt.tracks_id = ${el}
        )`).join(' INTERSECT ')};
    `;

    return await db.query(sql);
  },

  create: async (tracksIdsList, playlistId) => {
    const sql = `
    INSERT INTO playlists_tracks (playlists_id, tracks_id)
        VALUES ${tracksIdsList.map(trackId => `(${playlistId}, ${trackId})`).join(', ')};
    `;

    return await db.query(sql, tracksIdsList);
  },
};

export default playlistsTracksData;


import db from '../../config.js';

const artistsData = {

  get: async () => {
    const sql = `SELECT a.id, a.d_id AS deezerId, a.name, a.picture_medium_url AS pictureMediumUrl
                     FROM artists a;`;
    return await db.query(sql);
  },

  
};

export default artistsData;


import db from '../../config.js';

const tracksData = {
  getMultipleById: async (tracksIdsList) => {
    const sql = `
      SELECT t.*
      FROM tracks t
      WHERE t.id in (${tracksIdsList.map(id => `${id}`).join(', ')});
    `;
    return await db.query(sql, tracksIdsList);
  },

  getBy: async (column, value) => {
    const sql = `
    SELECT t.id, t.title, t.duration, g.name as genre_name, ar.name as name, a.title as album_title
    FROM tracks t
    JOIN artists ar ON ar.id=t.artists_id
    JOIN albums a ON a.id = t.albums_id
    JOIN genres g ON g.id = a.genres_id
    WHERE g.${column} = ?;
    `;

    return await db.query(sql, [value]);
  },

};

export default tracksData;
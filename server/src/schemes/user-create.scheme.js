
import { constants } from '../common/constants.js';
import { validateEmail, validatePassword, validateString } from '../common/validate-properties-enum.js';

export default {
  username: value => validateString(value, constants.nameMinLength, constants.nameMaxLength),
  password: value => validatePassword(value, constants.userPasswordMinLength, constants.userPasswordMaxLength)
    && validateString(value, 1, constants.passwordMaxLength),
  name: value => validateString(value, constants.nameMinLength, constants.nameMaxLength),
  email: value => validateEmail(value, constants.emailRegex) && validateString(value, 1, constants.nameMaxLength),
};

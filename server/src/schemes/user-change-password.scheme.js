
import { constants } from '../common/constants.js';
import { validatePassword, validateString } from '../common/validate-properties-enum.js';

export default {
  password: value => validatePassword(value, constants.userPasswordMinLength, constants.userPasswordMaxLength)
    && validateString(value, 1, constants.passwordMaxLength),
  newPassword: value => validatePassword(value, constants.userPasswordMinLength, constants.userPasswordMaxLength)
  && validateString(value, 1, constants.passwordMaxLength),
};

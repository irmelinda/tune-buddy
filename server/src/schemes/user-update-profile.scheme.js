import { constants } from '../common/constants.js';
import { validateEmail, validateString } from '../common/validate-properties-enum.js';

export default {
  username: value => !value || validateString(value, constants.nameMinLength, constants.nameMaxLength),
  name: value => !value || validateString(value, constants.nameMinLength, constants.nameMaxLength),
  email: value => !value || validateEmail(value, constants.emailRegex) && validateString(value, 1, constants.nameMaxLength),
};

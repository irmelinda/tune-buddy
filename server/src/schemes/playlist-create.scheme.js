import { constants } from '../common/constants.js';
import { validateNumber, validateString } from '../common/validate-properties-enum.js';

export default {
  name: value => validateString(value, constants.playlistsNameMinLength, constants.playlistsNameMaxLength),
  cover_url: value => validateString(value, constants.playlistsNameMinLength, constants.playlistsNameMaxLength),
  tracks_list: value => Array.isArray(value) && value.length > 0 && value.every(el => validateNumber(el)),
};

import { constants } from '../common/constants.js';
import { validateNumber, validateString } from '../common/validate-properties-enum.js';

export default {
  name: value => !value || validateString(value, constants.playlistsNameMinLength, constants.playlistsNameMaxLength),
  duration: value => !value || validateNumber(value),
  page: value => !value || validateNumber(value),
  playlistsCount: value => !value || validateNumber(value),
  direction: value => !value || (['asc', 'desc'].includes(value)),
  orderBy: value => !value || (['name', 'id', 'tracks_avg_rank', 'created_on'].includes(value)),
  users_id: value => !value || validateNumber(value),
};


import { validateNumber } from '../common/validate-properties-enum.js';

export default {

  duration: value => validateNumber(value) && value > 0,
  repeatArtist: value => value === true || value === false,
  genres: value => Array.isArray(value) && ((value.reduce((acc, el) => acc + el.percent, 0) === 100) || value.length === 0), // check all three elements of every single object

};

import { constants } from '../common/constants.js';
import { validateEmail, validatePassword, validateString } from '../common/validate-properties-enum.js';

export default {
  email: value => validateEmail(value, constants.emailRegex) && validateString(value, 1, constants.nameMaxLength),
  password: value => validatePassword(value, constants.userPasswordMinLength, constants.userPasswordMaxLength),
};


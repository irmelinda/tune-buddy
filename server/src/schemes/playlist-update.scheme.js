import { constants } from '../common/constants.js';
import { validateString } from '../common/validate-properties-enum.js';

export default {
  name: value => !value || validateString(value, constants.playlistsNameMinLength, constants.playlistsNameMaxLength),
  cover_url: value => !value || validateString(value, constants.playlistsNameMinLength, constants.playlistsNameMaxLength),
};

import db from '../../config.js';
import { DEEZER_URL } from '../common/constants.js';
import fetch from 'node-fetch';

const albumsCount = 15;
const artistsMaxCount = 15;

const insertTracksByAlbumId = async (tracks, albumId, artistId) => {
  const SQL_TRACKS = `
    INSERT INTO tracks (d_id, title, duration, rank, albums_id, artists_id)
    VALUES (?, ?, ?, ?, ?, ?);
  `;

  let index = 0;
  while (index < tracks.length) {
    const { id, title, duration, rank } = tracks[index];
    try {
      await db.query(SQL_TRACKS, [id, title, duration, rank, albumId, artistId]);
      console.log(`Seeding track with id ${id}.`);
    } catch (error) {
      error && console.log(error.code);
    }
    index++;
  }
};

const insertAlbumsByArtistId = async (albums, artistId, genreId) => {
  const SQL_ALBUMS = `
    INSERT INTO albums ( id, title, cover_medium_url, genres_id, artists_id )
    VALUES ( ?, ?, ?, ?, ?);
  `;

  let index = 0;
  while (index < albums.length && index < albumsCount) {
    const { id, title, cover_medium } = albums[index];

    try {
      await db.query(SQL_ALBUMS, [id, title, cover_medium, genreId, artistId]);
      console.log(`Seeding album with id ${id}.`);
    } catch (error) {
      error && console.log(error.code);
    }

    try {
      const res = await fetch(`${DEEZER_URL}/album/${id}/tracks`);
      const { data, error } = await res.json();
      error && console.log('Error from Deezer in insertAlbumsByArtistId ' + error.code);
      data && await insertTracksByAlbumId(data, id, artistId);
    } catch (error) {
      console.log(error);
    }
    index++;
  }
};

const insertArtistsByGenreId = async (artists, genreId) => {
  const SQL_ARTISTS = `
      INSERT INTO artists (id, name, picture_medium_url, genres_id)
      VALUES (?, ?, ?, ?);
    `;

  let index = 0;
  while (index < artists.length && index < artistsMaxCount) {
    const { id, name, picture_medium } = artists[index];

    try {
      await db.query(SQL_ARTISTS, [id, name, picture_medium, genreId]);
      console.log(`Seeding artist with id ${id}.`);
    } catch (error) {
      error && console.log(error.code);
    }

    try {
      const res = await fetch(`${DEEZER_URL}/artist/${id}/albums`);
      const { data, error } = await res.json();
      error && console.log('Error from Deezer in insertArtistsByGenreId ' + error.code);
      data && await insertAlbumsByArtistId(data, id, genreId);
    } catch (error) {
      console.log(error);
    }
    index++;
  }
};

const insertGenres = async (genres) => {
  const SQL_GENRES = `
    INSERT INTO genres ( id, name, picture_medium_url)
    VALUES ( ?, ?, ?);
  `;

  let index = 0;
  while (index < genres.length) {
    const { id, name, picture_medium } = genres[index];

    try {
      await db.query(SQL_GENRES, [id, name, picture_medium]);
      console.log(`Seeding genre with id ${id}.`);
    } catch (error) {
      error && console.log(error.code);
    }

    try {
      const res = await fetch(`${DEEZER_URL}/genre/${id}/artists`);
      const { data, error } = await res.json();
      error && console.log('Error from Deezer  in insertGenres ' + error.code);
      data && await insertArtistsByGenreId(data, id);
    } catch (error) {
      console.log(error);
    }
    index++;
  }
};

export const insertDataFromDeezer = async (ourGenres) => {
  try {
    const res = await fetch(`${DEEZER_URL}/genre`);
    const { data, error } = await res.json();
    error && console.log('Error from Deezer ' + error.code);
    data && await insertGenres(data.filter(genre => Object.keys(ourGenres).includes(String(genre.id))));
  } catch (error) {
    console.log(error);
  }
};
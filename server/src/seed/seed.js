import db from '../../config.js';
import { OUR_GENRES } from '../common/constants.js';
import { insertDataFromDeezer } from './insert-data-from-deezer.js';
import { insertUsers, insertRoles, insertPlaylists, insertPlaylistsTracks } from './insert-data-custom.js';

(async () => {
  console.log('To be seeded approximately 10 000 tracks for about 5 mins.');
  console.log('Data seed started...');
  await insertRoles();
  await insertUsers();
  await insertPlaylists();
  await insertDataFromDeezer(OUR_GENRES);
  await insertPlaylistsTracks();
  db.end();
  console.log('Seed finished!');
})()
  .catch(console.log);












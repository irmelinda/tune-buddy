import { USERS, ROLES, PLAYLISTS, PLAYLISTS_TRACKS } from '../common/constants.js';
import db from '../../config.js';
import bcrypt from 'bcrypt';

export const insertRoles = async () => {
  const SQL_ROLES_SELECT = `
        SELECT *
        FROM roles
        WHERE name = ?;
    `;

  const SQL_ROLES_INSERT = `
        INSERT INTO roles (id, name)
        VALUES (?, ?);
    `;

  await Promise.all(ROLES.map(async ({ id, name }) => {
    const resultSelect = await db.query(SQL_ROLES_SELECT, [id, name]);

    if (!resultSelect || resultSelect.length === 0) {
      await db.query(SQL_ROLES_INSERT, [id, name]);
    }
  }));

};

export const insertUsers = async () => {
  const SQL_USERS_SELECT = `
        SELECT *
        FROM users
        WHERE email = ? AND username = ?;
    `;

  const SQL_USERS_INSERT = `
           INSERT INTO users (username, password, name, email, roles_id, avatar_url, ban_expiration_date, is_deleted)
           VALUES (?, ?, ?, ?, ?, ?, ?, ?);
       `;

  await Promise.all(USERS.map(async ({ username, password, name, email, roles_id, avatar_url, ban_expiration_date, is_deleted }) => {
    const resultSelect = await db.query(SQL_USERS_SELECT, [email, username]);

    if (!resultSelect || resultSelect.length === 0) {
      await db.query(SQL_USERS_INSERT, [username, await bcrypt.hash(password, 10), name, email, roles_id, avatar_url, ban_expiration_date, is_deleted]);
    }
  }));
};

export const insertPlaylists = async () => {
  const SQL_PLAYLISTS_SELECT = `
    SELECT *
    FROM playlists
    WHERE name = ?;
`;

  const SQL_PLAYLISTS_INSERT = `
       INSERT INTO playlists (name, users_id, cover_url, created_on)
       VALUES (?, ?, ?, ?);
   `;
  await Promise.all(PLAYLISTS.map(async ({ name, users_id, cover_url, created_on }) => {
    const resultSelect = await db.query(SQL_PLAYLISTS_SELECT, [name]);

    if (!resultSelect || resultSelect.length === 0) {
      await db.query(SQL_PLAYLISTS_INSERT, [name, users_id, cover_url, created_on]);
    }
  }));
};

export const insertPlaylistsTracks = async () => {
  const SQL_PLAYLISTS_TRACKS_SELECT = `
      SELECT *
      FROM playlists_tracks
      WHERE tracks_id = ? AND playlists_id = ?;
  `;
  
  const SQL_PLAYLISTS_TRACKS_INSERT = `
         INSERT INTO playlists_tracks (tracks_id, playlists_id)
         VALUES (?, ?);
     `;
  await Promise.all(PLAYLISTS_TRACKS.map(async ({ tracks_id, playlists_id }) => {
    const resultSelect = await db.query(SQL_PLAYLISTS_TRACKS_SELECT, [tracks_id, playlists_id]);
  
    if (!resultSelect || resultSelect.length === 0) {
      await db.query(SQL_PLAYLISTS_TRACKS_INSERT, [tracks_id, playlists_id]);
    }
  }));
};
//import tracks, { genres, duration, repeatArtist } from '../../test/algo_const.js';
import { MAX_GENRES_PER_PLAYLIST } from '../common/constants.js';

/**
 * 
 * @param {*} duration  time in seconds
 * @param {*} repeat_art boolean - same artist in the playlist
 * @param {*} genres array
 */

const playlistAlgorithm = async (duration, repeatArtist, genres, tracksData, genresData) => { //repeat_art
  console.log(genresData);

  const playlist = new Set();
  const listOfArtistIds = new Set();
  let tripDuration = duration;


  if (genres.length === 0) {
    const genresFromDB = await genresData.getAll();
    console.log(genresFromDB);
    while (genres.length < MAX_GENRES_PER_PLAYLIST) {
      const getIndex = Math.floor(Math.random() * (MAX_GENRES_PER_PLAYLIST - 0) + 0);
      genres.push({ genres_id: genresFromDB[getIndex].id, percent: Math.floor(100 / MAX_GENRES_PER_PLAYLIST) });
    }

  }
  for (let genre of genres) {
    //const filteredTracks = tracks.filter(track => track.genres_id === genre.genres_id);
    const filteredTracks = await tracksData.getBy('id', genre.genres_id);
    //console.log(filteredTracks);
    const existingTrackDuration = filteredTracks.reduce((acc, el) => acc + el.duration, 0);
    //console.log('existing tracks duration ' + existingTrackDuration);
    // console.log('travel duration ' + duration);
    // console.log('percent per genre' + item.percent);

    let genreTime = Math.floor(duration * (genre.percent / 100));  // time per genre in seconds
    if (genreTime < existingTrackDuration) {
      //console.log('duration per genre ' + genreTime);

      while (genreTime > 0 && tripDuration > 0) {
        const getIndex = Math.floor(Math.random() * (filteredTracks.length - 0) + 0);


        const addToPlaylist = (getIndex) => {

          playlist.add(filteredTracks[getIndex]);
          genreTime -= filteredTracks[getIndex].duration;
          tripDuration -= filteredTracks[getIndex].duration;
          filteredTracks.splice(getIndex, 1);
        };

        if (repeatArtist === false) {
          listOfArtistIds.add(filteredTracks[getIndex].artist_id);

          // if (!listOfArtistIds.has(filteredTracks[getIndex].artist_id)) {
          addToPlaylist(getIndex);
          // }
        } else {
          addToPlaylist(getIndex);
        }
      }
    } else {
      console.log(`Not enough tracks in genre ${genre.genre}`);
    }
  }

  const playlistArray = Array.from(playlist);
  const playlistDuration = playlistArray.reduce((acc, el) => acc + el.duration, 0);
  console.log('playlist duration ' + playlistDuration);
  const shuffled = playlistArray.sort(() => Math.random() - 0.5);
  return shuffled;
  //return playlist;
};

// const res = playlistAlgorithm(duration, repeatArtist, genres);

// console.log(res);


export default playlistAlgorithm;
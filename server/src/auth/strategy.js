import passportJwt from 'passport-jwt';
import { SECRET_KEY } from '../../config.js';

const options = {
  secretOrKey: SECRET_KEY, // no one could modify the token without the secret key
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(), 
};

// the payload will be injected from the decoded token
const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {

  const user = {
    id: payload.sub,
    username: payload.username,
    email: payload.email,
    role: payload.role,
  };

  // the user object will be injected in the request object and can be accessed as req.user in authenticated controllers
  done(null, user);
});

export default jwtStrategy;

import jwt from 'jsonwebtoken';
import { SECRET_KEY, TOKEN_DURATION } from '../../config.js';

const createToken = (payload) => {
  const token = jwt.sign(
    payload,
    SECRET_KEY,
    // { expiresIn: TOKEN_DURATION },
  );

  return token;
};

export default createToken;

const serviceMessages = {
  EXISTS: 'Field already exists in the database.',
  DOES_NOT_EXIST: 'Search does not exist in the database.',
  
  DUPLICATE: 'Input data already exists in the database.',

  RETRIEVED: 'Successfully retrieved.',

  CREATED: 'Successfully created.',
  UPDATED: 'Successfully updated.',
  DELETED: 'Successfully deleted.',

  GENERATED: 'Successfully generated.',

  RESTRICTED: 'Restricted access.',
  INVALID: 'Invalid data',
  BANNED: 'User banned.',
  SUCCESS: 'Success.',
  UNAUTHORIZED: 'Unauthorized.',
};

export default serviceMessages;
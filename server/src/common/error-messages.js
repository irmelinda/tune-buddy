import { constants } from './constants.js';

export default {
  createUser: {
    username: `Expected string with length > ${constants.nameMinLength}  and no more than ${constants.nameMaxLength}.`,
    password: `Expected string with length > ${constants.userPasswordMinLength}  and no more than ${constants.userPasswordMinLength} which includes at least one integer.`,
    name: `Expected string with length > ${constants.nameMinLength} and no more than ${constants.nameMaxLength}.`,
    email: 'Expected valid email.',
  },
  usersLogin: {
    email: 'Expected valid email.',
    password: 'Entered password doesn\'t match user\'s registration password.',
  },
  params: {
    params: 'Expected integers.',
  },
  changePassword: {
    password: `Expected string with length > ${constants.userPasswordMinLength}  and no more than ${constants.userPasswordMinLength} which includes at least one integer.`,
    newPassword: `Expected string with length > ${constants.userPasswordMinLength}  and no more than ${constants.userPasswordMinLength} which includes at least one integer.`,
  },
  updateProfile: {
    username: `Expected string with length > ${constants.nameMinLength}  and no more than ${constants.nameMaxLength}.`,
    name: `Expected string with length > ${constants.nameMinLength} and no more than ${constants.nameMaxLength}.`,
    email: 'Expected valid email.',
  },
  playlistQuery: {
    page: 'Expected only one integer.',
    playlistsCount: 'Expected only one integer.',
    direction: 'Expected to be "asc" or "desc".',
    orderBy: 'Expected to be one of these: "name", "id", "tracks_avg_rank" or "created_on".',
    users_id: 'Expected only one integer.',
  },
  playlistCreate: {
    name: `Expected string with length > ${constants.playlistsNameMinLength} and no more than ${constants.playlistsNameMaxLength}.`,
    cover_url: `Expected string with length > ${constants.playlistsNameMinLength} and no more than ${constants.playlistsNameMaxLength}.`,
    tracks_list: 'Expected non empty array of integers of track ids.',
  },
  playlistUpdate: {
    name: `Expected string with length > ${constants.playlistsNameMinLength} and no more than ${constants.playlistsNameMaxLength}.`,
    cover_url: `Expected string with length > ${constants.playlistsNameMinLength} and no more than ${constants.playlistsNameMaxLength}.`,
  },
  tracksGenerator: {
    duration: 'Expected only one integer.',
    repeatArtist: 'Expected boolean',
    genres: 'Expected array and sum of genre percentage equal to 100.',
  },
};



export const DEEZER_URL = 'https://api.deezer.com';
// secret key obtained from Deezer
//const DEEZER_SECRET_KEY = '9e8ef4dddf246f4ff90e3f692d54b13a';
//application ID obtained from Deezer (fyi)
//const APPLICATION_ID = '479782';

export const BING_MAPS_URL = 'http://dev.virtualearth.net/REST/v1/Routes/Driving';
export const APIKEY_BING_MAPS = 'AuxK6BbN2mwTC8QOMUGArI661OQhEEzEish-MypKlQh0pwx2WhRKToWLYjGMeYaE';

export const MAX_GENRES_PER_PLAYLIST = 3;
export const OUR_GENRES = {
  85: 'Alternative',
  106: 'Electro',
  116: 'Rap/Hip Hop',
  129: 'Jazz',
  152: 'Rock',
  169: 'Soul & Funk',
};

export const PLAYLISTS_TRACKS = [
  // Playlist id 1
  {
    tracks_id: 3,
    playlists_id: 1,
  },
  {
    tracks_id: 200,
    playlists_id: 1,
  },
  {
    tracks_id: 450,
    playlists_id: 1,
  },
  {
    tracks_id: 900,
    playlists_id: 1,
  },
  {
    tracks_id: 6000,
    playlists_id: 1,
  },
  {
    tracks_id: 800,
    playlists_id: 1,
  },
  {
    tracks_id: 6845,
    playlists_id: 1,
  },
  // Playlist id 2
  {
    tracks_id: 3333,
    playlists_id: 2,
  },
  {
    tracks_id: 145,
    playlists_id: 2,
  },
  {
    tracks_id: 889,
    playlists_id: 2,
  },
  {
    tracks_id: 1234,
    playlists_id: 2,
  },
  {
    tracks_id: 7894,
    playlists_id: 2,
  },
  {
    tracks_id: 2577,
    playlists_id: 2,
  },
  {
    tracks_id: 8888,
    playlists_id: 2,
  },
  {
    tracks_id: 1,
    playlists_id: 2,
  },
  // Playlist id 3
  {
    tracks_id: 3335,
    playlists_id: 3,
  },
  {
    tracks_id: 1455,
    playlists_id: 3,
  },
  {
    tracks_id: 8895,
    playlists_id: 3,
  },
  {
    tracks_id: 1678,
    playlists_id: 3,
  },
  {
    tracks_id: 4567,
    playlists_id: 3,
  },
  {
    tracks_id: 4455,
    playlists_id: 3,
  },
  {
    tracks_id: 6666,
    playlists_id: 3,
  },
  {
    tracks_id: 1256,
    playlists_id: 3,
  },
  {
    tracks_id: 8901,
    playlists_id: 3,
  },
  {
    tracks_id: 245,
    playlists_id: 3,
  },
  {
    tracks_id: 889,
    playlists_id: 3,
  },
  {
    tracks_id: 1334,
    playlists_id: 3,
  },
  {
    tracks_id: 7994,
    playlists_id: 3,
  },
  {
    tracks_id: 2677,
    playlists_id: 3,
  },
  {
    tracks_id: 8988,
    playlists_id: 3,
  },
  {
    tracks_id: 2,
    playlists_id: 3,
  },
  {
    tracks_id: 7582,
    playlists_id: 4,
  },
  {
    tracks_id: 196,
    playlists_id: 4,
  },
  {
    tracks_id: 205,
    playlists_id: 4,
  },
  {
    tracks_id: 907,
    playlists_id: 4,
  },
  {
    tracks_id: 906,
    playlists_id: 4,
  },
  {
    tracks_id: 245,
    playlists_id: 4,
  },
  {
    tracks_id: 8388,
    playlists_id: 4,
  },
  {
    tracks_id: 2,
    playlists_id: 4,
  },
  {
    tracks_id: 7582,
    playlists_id: 5,
  },
  {
    tracks_id: 196,
    playlists_id: 5,
  },
  {
    tracks_id: 2055,
    playlists_id: 5,
  },
  {
    tracks_id: 987,
    playlists_id: 5,
  },
  {
    tracks_id: 1200,
    playlists_id: 5,
  },
  {
    tracks_id: 2334,
    playlists_id: 5,
  },
  {
    tracks_id: 7532,
    playlists_id: 5,
  },
  {
    tracks_id: 4578,
    playlists_id: 5,
  },
  {
    tracks_id: 7522,
    playlists_id: 6,
  },
  {
    tracks_id: 5526,
    playlists_id: 6,
  },
  {
    tracks_id: 3555,
    playlists_id: 6,
  },
  {
    tracks_id: 9857,
    playlists_id: 6,
  },
  {
    tracks_id: 1204,
    playlists_id: 6,
  },
  {
    tracks_id: 1200,
    playlists_id: 7,
  },
  {
    tracks_id: 2334,
    playlists_id: 7,
  },
  {
    tracks_id: 7532,
    playlists_id: 7,
  },
  {
    tracks_id: 4578,
    playlists_id: 7,
  },
  {
    tracks_id: 7522,
    playlists_id: 7,
  },
  {
    tracks_id: 5526,
    playlists_id: 7,
  },
  {
    tracks_id: 3555,
    playlists_id: 7,
  },
  {
    tracks_id: 9857,
    playlists_id: 7,
  },
  {
    tracks_id: 1204,
    playlists_id: 7,
  },
];

export const PLAYLISTS = [
  {
    name: 'To The Beach',
    users_id: 1,
    cover_url:'http://localhost:5555/playlists_covers/to_the_beach.jpg',
    created_on:'2021-01-01',
  },
  {
    name: 'Road To Climbing The Swiss Alps',
    users_id: 1,
    cover_url: 'http://localhost:5555/playlists_covers/road_to_climbing_the_swiss_alps.jpg',
    created_on:'2021-02-01',
  },
  {
    name: 'Morning Jam To Work',
    users_id: 2,
    cover_url: 'http://localhost:5555/playlists_covers/morning_jam_to_work.jpg',
    created_on:'2021-03-01',
  },
  {
    name: 'Varna Sofia',
    users_id: 3,
    cover_url: 'http://localhost:5555/playlists_covers/varna_sofia.jpg',
    created_on:'2021-04-01',
  },
  {
    name: 'Greece Road Trip',
    users_id: 3,
    cover_url: 'http://localhost:5555/playlists_covers/greece_road_trip.jpg',
    created_on:'2021-05-01',
  },
  {
    name: 'Disco Night In The Car',
    users_id: 4,
    cover_url: 'http://localhost:5555/playlists_covers/disco_night_in_the_car.jpg',
    created_on:'2021-06-01',
  },
  {
    name: 'Highway to Vienna',
    users_id: 5,
    cover_url: 'http://localhost:5555/playlists_covers/highway_to_vienna.jpg',
    created_on:'2021-07-01',
  },
];

export const ROLES = [
  {
    id:1,
    name:'admin',
  },
  {
    id:2,
    name:'member',
  },
];
export const USERS = [
  {
    username: 'dimitriya',
    password: 'Dimitriya2021',
    name: 'Dimitriya Vardzhieva',
    email: 'dimitriya.v@tune_buddy.com',
    roles_id: 1,
    avatar_url: 'http://localhost:5555/avatars/dimitriya_vardzhieva.jpg',
    ban_expiration_date: null,
    is_deleted:0,
  },
  {
    username: 'irma',
    password: 'Irma2021',
    name: 'Irma Atanassova',
    email: 'irma.a@tune_buddy.com',
    roles_id: 1,
    avatar_url: 'http://localhost:5555/avatars/irma_atanassova.jpg',
    ban_expiration_date: null,
    is_deleted:0,
  },
  {
    username: 'billie',
    password: 'Billie2021',
    name: 'Billie Eilish',
    email: 'b.eilish@virginia.com',
    roles_id: 2,
    avatar_url: 'http://localhost:5555/avatars/billie_eilish.jpg',
    ban_expiration_date: null,
    is_deleted:0,
  },
  {
    username: 'ed',
    password: 'Ed2021',
    name: 'Ed Sheeran',
    email: 'ed.sheeran@london.com',
    roles_id: 2,
    avatar_url: 'http://localhost:5555/avatars/paul_david_hewson.jpg',
    ban_expiration_date: null,
    is_deleted:0,
  },
  {
    username: 'bono',
    password: 'Bono2021',
    name: 'Paul David Hewston',
    email: 'bono@u2.com',
    roles_id: 2,
    avatar_url: 'http://localhost:5555/avatars/paul_david_hewson.jpg',
    ban_expiration_date: null,
    is_deleted:0,
  },
  {
    username: 'whitney',
    password: 'Whitney2021',
    name: 'Whitney Houston',
    email: 'whitney@whitney.houston.com',
    roles_id: 2,
    avatar_url: 'http://localhost:5555/avatars/whitney_houston.jpg',
    ban_expiration_date: null,
    is_deleted: 1,
  },
  {
    username: 'voldemort',
    password: 'Voldemort2021',
    name: 'Tom Riddle',
    email: 'voldemort@harrypotter.com',
    roles_id: 2,
    avatar_url: 'http://localhost:5555/avatars/tom_riddle.jpg',
    ban_expiration_date: '2021-07-25',
    is_deleted:0,
  },
];



export const constants = {
  nameMinLength: 2,
  nameMaxLength: 45,
  userPasswordMinLength: 4,
  userPasswordMaxLength: 45,
  // emailRegex : /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-_]+)*$/,
  emailRegex : /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9-]+)*$/,
  passwordRegex: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 
  passwordMaxLength :255,
  usersCountMin: 100,
  usersCountMax: 1000,

  playlistsCountMinValue:3,
  playlistsCountMaxValue:500,
  playlistsNameMinLength: 2,
  playlistsNameMaxLength: 255,

};

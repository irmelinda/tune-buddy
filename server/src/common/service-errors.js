const serviceErrors = {
  /** Such a record does not exist (when it is expected to exist) */
  RECORD_NOT_FOUND: 404,
  /** The requirements do not allow more than one of that resource */
  DUPLICATE_RECORD: 400,
  /** The requirements do not allow such an operation */
  OPERATION_NOT_PERMITTED: 409, // 403
  /** username/password mismatch */
  INVALID_SIGN_IN: 400,
};

export default serviceErrors;


export const validateString = (value, lengthMin, lengthMax) => typeof value === 'string' && value.length > lengthMin && value.length < lengthMax;

export const validatePassword = (value, lengthMin, lengthMax) => {
  return typeof value === 'string'
    && value.length > lengthMin
    && value.length < lengthMax
    && Array.from(value).some(el => validateNumber(el));
};

export const validateEmail = (value, emailRegex) => emailRegex.test(value);

export const validateNumber = value => typeof Number(value) && !isNaN(+value);



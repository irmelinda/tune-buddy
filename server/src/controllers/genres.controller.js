import express from 'express';
import playlistsServices from '../services/playlists.services.js';
import serviceErrors from '../common/service-errors.js';
import genresData from '../data/genres.data.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import isBannedValidator from '../middlewares/is-banned-validation.js';
import validateLoggedUser from '../middlewares/validate.logged.user.js';
import isDeletedValidator from '../middlewares/is-deleted-validation.js';

const genresController = express.Router();
genresController.use(authMiddleware, validateLoggedUser, isBannedValidator, isDeletedValidator);

// 0. Retrieve genres
genresController.get('/',
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      getPlaylistsGenres(genresData)();

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

export default genresController;
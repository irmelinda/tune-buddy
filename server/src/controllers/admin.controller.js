import express from 'express';
import validateParamsMiddleware from '../middlewares/validate-params.middleware.js';
import usersServices from '../services/users.services.js';
import { authMiddleware, roleMiddleware } from '../auth/auth.middleware.js';
import serviceErrors from '../common/service-errors.js';
import usersData from '../data/users.data.js';
import validateLoggedUser from '../middlewares/validate.logged.user.js';
import playlistsData from '../data/playlists.data.js';
import playlistsServices from '../services/playlists.services.js';
import playlistQueryScheme from '../schemes/playlist-query.scheme.js';
import transformQueryMiddleware from '../middlewares/transform-query.middleware.js';
import validateQuery from '../middlewares/validate-query.js';

const adminController = express.Router();
adminController.use(authMiddleware, validateLoggedUser/* , roleMiddleware('admin') */);

// 1. get all user
adminController.get('/users', async (req, res) => {
  const { error, data, message } = await usersServices
    .getAllUsers(usersData)(req.query); //getAllUsers(usersData)(req.query);
  console.log('log in controller');
  console.log(data);
  if (error === serviceErrors.RECORD_NOT_FOUND) {
    res.status(404).send({ message, error });
  } else {
    res.status(200).send({ message, data });
  }
});

// 2. view single user (profile)
adminController.get('/users/:id', async (req, res) => {
  const { error, data, message } = await usersServices
    .getUserById(usersData)(req.params.id);
  // console.log('log in controller');
  // console.log(data);
  if (error === serviceErrors.RECORD_NOT_FOUND) {
    res.status(404).send({ message, error });
  } else {
    res.status(200).send({ message, data });
  }
});

// 3. update user's role

adminController.put('/users/:id/update', // or route should be /:id/update-role
  async (req, res) => {
    console.log('id from params' + req.params.id);
    //logic here
    const { error, data, message } = await usersServices.updateUserRole(usersData)(req.body, req.params.id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message, error });
    }
    res.status(200).send({ message, data });
  });


// 4. Ban user 
adminController.put('/users/:id',
  validateParamsMiddleware,
  async (req, res) => {
    const { error, data, message } = await usersServices
      .banUser(usersData)(req.params.id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message, error });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 5. Delete user 
adminController.delete('/users/:id',
  validateParamsMiddleware,
  async (req, res) => {
    const { error, data, message } = await usersServices
      .deleteUser(usersData)(+req.params.id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message, error });
    } else if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message, error });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 6.1. Retrieve playlists
adminController.get('/playlists',
  transformQueryMiddleware(playlistQueryScheme),
  validateQuery('playlistQuery', playlistQueryScheme),
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      getPlaylists(playlistsData)(req.query, req.user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 6.2. Retrieve playlist by id
adminController.get('/playlists/:id',
  validateParamsMiddleware,
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      getPlaylistById(playlistsData)(+req.params.id, req.user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 6.3 Create playlist - in playlists controller

// 6.4. Update playlist - in playlists controller

// 7. Delete playlist
adminController.delete('/playlists/:id',
  validateParamsMiddleware,
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      delete(playlistsData)(+req.params.id, req.user);

      
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

export default adminController;
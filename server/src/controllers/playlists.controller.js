import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import isBannedValidator from '../middlewares/is-banned-validation.js';
import validateLoggedUser from '../middlewares/validate.logged.user.js';
import isDeletedValidator from '../middlewares/is-deleted-validation.js';
import transformQuery from '../middlewares/transform-query.middleware.js';
import validateQuery from '../middlewares/validate-query.js';
import playlistQueryScheme from '../schemes/playlist-query.scheme.js';
import playlistsServices from '../services/playlists.services.js';
import playlistsData from '../data/playlists.data.js';
import serviceErrors from '../common/service-errors.js';
import validateParamsMiddleware from '../middlewares/validate-params.middleware.js';
import validateBody from '../middlewares/validate-body.middleware.js';
import transformBody from '../middlewares/transform-body.middleware.js';
import createPlaylistScheme from '../schemes/playlist-create.scheme.js';
import updatePlaylistScheme from '../schemes/playlist-update.scheme.js';
import playlistsTracksServices from '../services/playlists-tracks.services.js';
import playlistsTracksData from '../data/playlists-tracks.data.js';
import tracksData from '../data/tracks.data.js';
import generatorServices from '../services/generator.services.js';
import tracksGeneratorScheme from '../schemes/tracks-generator.scheme.js';
import genresData from '../data/genres.data.js';

const playlistsController = express.Router();
playlistsController.use(authMiddleware, validateLoggedUser, isBannedValidator, isDeletedValidator);

// 0. Generate tracks
playlistsController.post('/generator',
  validateBody('tracksGenerator', tracksGeneratorScheme),
  transformBody(tracksGeneratorScheme),
  async (req, res) => {

    const { error, data, message } = await generatorServices.
      generatePlaylist(tracksData, genresData)(req.body);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 1. Retrieve playlists
playlistsController.get('/',
  transformQuery(playlistQueryScheme),
  validateQuery('playlistQuery', playlistQueryScheme),
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      getPlaylists(playlistsData)(req.query, req.user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 2. Retrieve playlist by id
playlistsController.get('/:id',
  validateParamsMiddleware,
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      getPlaylistById(playlistsData)(+req.params.id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 3. Create playlist
playlistsController.post('/',
  validateBody('playlistCreate', createPlaylistScheme),
  transformBody(createPlaylistScheme),
  async (req, res) => {

    const { /* error, */ data, message } = await playlistsServices.
      createPlaylist(playlistsData, playlistsTracksData)(req.body, req.user);

    // if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
    //   res.status(409).send({ message }); //403 not authorized
    // } else {
    res.status(200).send({ message, data });
    // }
  });

// 4. Update playlist
playlistsController.put('/:id',
  validateParamsMiddleware,
  validateBody('playlistUpdate', updatePlaylistScheme),
  transformBody(updatePlaylistScheme),
  async (req, res) => {
    const { error, data, message } = await playlistsServices
      .updatePlaylist(playlistsData)(+req.params.id, req.body, req.user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res.status(409).send({ message });
    } else if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(400).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 5. Delete playlist
playlistsController.delete('/:id',
  validateParamsMiddleware,
  async (req, res) => {
    const { error, data, message } = await playlistsServices
      .deletePlaylist(playlistsData)(+req.params.id, req.user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 6. Retrieve tracks list by playlist id
playlistsController.get('/:id/tracks',
  validateParamsMiddleware,
  async (req, res) => {

    const { error, data, message } = await playlistsTracksServices.
      getTracksByPlaylistId(playlistsData, playlistsTracksData)(+req.params.id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });


export default playlistsController;
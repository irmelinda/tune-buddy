import express from 'express';
import transformQuery from '../middlewares/transform-query.middleware.js';
import validateQuery from '../middlewares/validate-query.js';
import playlistQueryScheme from '../schemes/playlist-query.scheme.js';
import playlistsServices from '../services/playlists.services.js';
import playlistsData from '../data/playlists.data.js';
import serviceErrors from '../common/service-errors.js';
import validateParamsMiddleware from '../middlewares/validate-params.middleware.js';
import playlistsTracksServices from '../services/playlists-tracks.services.js';
import playlistsTracksData from '../data/playlists-tracks.data.js';

const publicController = express.Router();

// 1. Retrieve playlists
publicController.get('/',
  transformQuery(playlistQueryScheme),
  validateQuery('playlistQuery', playlistQueryScheme),
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      getPlaylists(playlistsData)(req.query);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

// 2. Retrieve playlist by id
publicController.get('/:id',
  validateParamsMiddleware,
  async (req, res) => {

    const { error, data, message } = await playlistsServices.
      getPlaylistById(playlistsData)(+req.params.id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });


// 5. Retrieve tracks list by playlist id
publicController.get('/:id/tracks',
  validateParamsMiddleware,
  async (req, res) => {

    const { error, data, message } = await playlistsTracksServices.
      getTracksByPlaylistId(playlistsData, playlistsTracksData)(+req.params.id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message });
    } else {
      res.status(200).send({ message, data });
    }
  });

export default publicController;
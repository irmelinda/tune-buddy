import express from 'express';
import usersServices from '../services/users.services.js';
import usersData from '../data/users.data.js';
import serviceErrors from '../common/service-errors.js';
import validateBody from '../middlewares/validate-body.middleware.js';
import createUserScheme from '../schemes/user-create.scheme.js';
import userLoginScheme from '../schemes/user-login.scheme.js';
import createToken from '../auth/create.token.js';
import validateLoggedUser from '../middlewares/validate.logged.user.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import changePasswordScheme from '../schemes/user-change-password.scheme.js';
import updateProfileScheme from '../schemes/user-update-profile.scheme.js';
import transformBody from '../middlewares/transform-body.middleware.js';
import multer from 'multer';
import path from 'path';
import serviceMessages from '../common/service-messages.js';

const usersController = express.Router();

// 1. Register - create user
usersController.post('/register',
  transformBody(createUserScheme),
  validateBody('createUser', createUserScheme),
  async (req, res) => {
    const createData = req.body;

    const { error, data, message } = await usersServices
      .createUser(usersData)(createData);
    if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message, error });
    } else {
      res.status(201).send({ message, data });
    }
  });

// 2. Login 
usersController.post('/login',
  transformBody(userLoginScheme),
  validateBody('usersLogin', userLoginScheme),
  async (req, res) => {
    console.log(req.body);
    const { email, password } = req.body;

    const { error, data, message } = await usersServices
      .signInUser(usersData)(email, password);

    if (error === serviceErrors.INVALID_SIGN_IN) {
      res.status(400).send({ message, error });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res.status(403).send({ message, error });
    } else {
      const payload = {
        sub: data.id,
        username: data.username,
        role: data.role,
        avatar_url: data.avatar_url,
        email: data.email,
        name: data.name,
      };

      const token = createToken(payload);

      res.status(200).send({
        token: token,
        data: data,
      });
    }
  });

// 3. Logout
usersController.delete('/logout',
  authMiddleware,
  validateLoggedUser,
  async (req, res) => {

    const { error, data, message } = await usersServices.logoutUser(usersData)(req.headers.authorization.replace('Bearer ', ''));
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(400).send({ message, error });
    }
    res.status(200).send({ message, data });
  });

// 4. My account  
usersController.get('/my-account',
  // validateTokenExpiredMiddleware,
  authMiddleware,
  validateLoggedUser,
  async (req, res) => {
    const { error, data, message } = await usersServices.getMyAccount(usersData)(req.user);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message, error });
    }
    console.log(data);
    res.status(200).send({ message, data });
  });

// 5. Change password
usersController.put('/change-password', // or route should be /:id/change-password
  authMiddleware,
  validateLoggedUser,
  validateBody('changePassword', changePasswordScheme),
  async (req, res) => {

    const { error, data, message } = await usersServices.updateUserPassword(usersData)(req.body, req.user);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message, error });
    }
    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res.status(403).send({ message, error });
    }
    res.status(200).send({ message, data });
  });

// 6. Update profile
usersController.put('/update-profile', // or route should be /:id/update-profile
  authMiddleware,
  validateLoggedUser,
  validateBody('updateProfile', updateProfileScheme),
  async (req, res) => {
    //logic here
    const { error, data, message } = await usersServices.updateUserProfile(usersData)(req.body, req.user.id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message, error });
    }
    res.status(200).send({ message, data });
  });

// 7. Upload avatars

const storage = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, 'avatars');
  },
  filename(req, file, callback) {
    const filename = Date.now() + path.extname(file.originalname);
    callback(null, filename);
  },
});

const upload = multer({ storage });
const type = upload.single('file');
usersController.put('/avatar',
  authMiddleware,
  validateLoggedUser,
  type,
  async (req, res) => {
    console.log('log in upload file');
    const { error, data, message } = await usersServices.updateUserAvatar(usersData)(req.file.filename, req.user.id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message, error });
    }
    res.status(200).send({ message, data });
    //res.status(200).send({ message: serviceMessages.SUCCESS });
  });

export default usersController;

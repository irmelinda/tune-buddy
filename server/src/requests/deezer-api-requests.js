import { DEEZER_URL } from '../common/constants.js';
import fetch from 'node-fetch';

export const loadGenres = async () => {

  const response = await fetch(`${DEEZER_URL}/genre`);
  return response.json();
};

export const loadArtistsByGenreId = async (genreId) => {

  const response = await fetch(`${DEEZER_URL}/genre/${genreId}/artists`);
  return response.json();
};

export const loadAlbumsByArtistId = async (artistId) => {

  const response = await fetch(`${DEEZER_URL}/artist/${artistId}/albums`);
  return response.json();
};

export const loadTracksByAlbumId = async (albumId) => {

  const response = await fetch(`${DEEZER_URL}/album/${albumId}/tracks`);
  return response.json();
};



import { BING_MAPS_URL, APIKEY_BING_MAPS } from '../common/constants.js';
import fetch from 'node-fetch';

export const loadLocation = async (startPoint, endPoint) => {
  try {
    const response = await fetch(`${BING_MAPS_URL}?wp.1=${startPoint}&wp.2=${endPoint}&key=${APIKEY_BING_MAPS}`);

    const result = await response.json();
    //console.log(result);
    if (result.statusCode === 200) {
      
      return result;
    } else {
      throw new Error(result.statusDescription);
    }
  } catch (e) {
    console.error(e.name +':'  + e.message);
  } 
};
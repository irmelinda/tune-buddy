import dotenv from 'dotenv';
import mariadb from 'mariadb';

const config = dotenv.config().parsed;

const db = mariadb.createPool(
  {
    port: config.DBPORT,
    database: config.DATABASE,
    user: config.USER,
    password: config.PASSWORD,
    host: config.HOST,
  },
);
export default db;
export const PORT = config.PORT;

export const SECRET_KEY = 'Tune_17.05.2021-Buddy';

//token duration before it expires in hours
export const TOKEN_DURATION = 60 * 60;

// Period of time a user will be banned from the web site
// 20 days
export const USER_BAN_PERIOD = 20 * 24 * 3600 * 1000;
export const MLS_TO_DAYS = 1000 * 60 * 60 * 24;


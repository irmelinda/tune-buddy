***Backend***


# Tune Buddy API

### 1. Description

Tune Buddy is web and mobile based music app that allows users to create playlist based on the duration of their trip. It offers variety of music genres and generates playlists based on users genre preferences.

Here, you can find detailed information and some requirements about our backend API.

Work is still in progress, so stay tuned :)

<br>

### 2. Project information and requirements

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**, **mariaDB**

<br>

### 3. Functionalities

The user can register, log in and log out.
Almost all features of the app are accessible after login, except the main/home page. 
The core functionality is a playlist generator. At first, it allows the user to set destination point. On step two we allow the user to select randomly generated cover picture for the playlist. Next step gives the user the possibility to select up to three music genres or leave this selection blank, in which case the algorithm select three random genres. At this step the user can also decide if he/she wants to have more tracks from one artist or wants only unique artists. We offer a preview of the playlist and the possibility to generate new tracks using the above criteria. At the end the user can save, play or discard the playlist.

For registered users we offer the possibility to manage their accounts - change personal information, view or edit playlists. This functionalities are partially implemented in our frontend.

Admin part allows the users logged with admin role to access a special view in the application. It gives information about all registered users, and offers some possibilities to manage them (e.g. ban of delete user or admin profiles, delete or edit users playlists).


<br>

### 4. Setup

Server folder is the **root** folder of the backend, where the `package.json` is placed.

You need to install all the packages in the root folder using `npm i`.

Create a new `.env` file in the root folder, and provide the following information in it:

```js PORT=5555
      HOST=localhost
      DBPORT=3306
      USER=root
      PASSWORD= // add your password to connect to mariaDb
      DATABASE=ibc
```
In the database folder you can find a script `tune_buddy_create_tables.sql`. Start it in your database visual tool to create the project's database.

To fill the database with all data needed to use the app run `npm run seed`. It takes approximately 4 minutes to fully set the database.

To run the project use `npm start` or `npm run start:dev` commands.

<br>

### 5. Project structure

We structured our application based on our knowledge on backend architecture, layered design and programming principles. 

- `config.js` - contains constants and db setup
- `logs/requests.json` - contains all logs from the server requests
- `src/index.js` - the entry point of the project
- `src/auth` - contains user authentication
- `src/common` - contains constants and error messages
- `src/controllers` - contains all controllers
- `src/data` - contains all requests to the database
- `src/middlewares` - contains all custom middleware
- `src/services` - contains all services
- `src/schemes` - contains objects (or schemas) for validating body/query objects
- `src/requests` - contains requests to external APIs
- `src/algorithm` - contains the playlist's algorithm

<br>

### 6. Postman

You are provided with a postman collection to ease you testing. You can find it in `postman` folder.
Fill free to experiment with the requests, we will be happy to receive your feedback :).

### 7. Project resources and data format - IN PROGRESS...

Tune Buddy API returns all response data as a JSON object, except for Unauthorized.
The API exposes the following resources and sub-resources

METHOD	ACTION
GET	Retrieves resources
POST	Creates resources
PUT	Changes and/or replaces resources or collections
DELETE	Deletes resources

#### 7.1 Admin

localhost:5555/admin/users?orderBy=id&direction=desc



<br>



#### Authentication
All requests to Tune Buddy API require authentication. This is achieved by sending a valid JWT access token in the request header.
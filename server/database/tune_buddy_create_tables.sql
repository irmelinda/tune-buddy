-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema tune_buddy
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tune_buddy
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tune_buddy` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema tune_buddy
-- -----------------------------------------------------
USE `tune_buddy` ;

-- -----------------------------------------------------
-- Table `tune_buddy`.`genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`genres` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `picture_medium_url` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tune_buddy`.`artists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`artists` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `picture_medium_url` VARCHAR(255) NOT NULL,
   `genres_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_artists_genres_idx` (`genres_id` ASC) VISIBLE,
  CONSTRAINT `fk_artists_genres`
    FOREIGN KEY (`genres_id`)
    REFERENCES `tune_buddy`.`genres` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tune_buddy`.`albums`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`albums` (
  `id` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `cover_medium_url` VARCHAR(255) NOT NULL,
  `genres_id` INT NOT NULL,
  `artists_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_albums_genres_idx` (`genres_id` ASC) VISIBLE,
  INDEX `fk_albums_artists1_idx` (`artists_id` ASC) VISIBLE,
  CONSTRAINT `fk_albums_genres`
    FOREIGN KEY (`genres_id`)
    REFERENCES `tune_buddy`.`genres` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_albums_artists1`
    FOREIGN KEY (`artists_id`)
    REFERENCES `tune_buddy`.`artists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tune_buddy`.`tracks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`tracks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `d_id` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `duration` INT NOT NULL,
  `rank` INT NOT NULL,
  `albums_id` INT NOT NULL,
  `artists_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tracks_albums1_idx` (`albums_id` ASC) VISIBLE,
  INDEX `fk_tracks_artists1_idx` (`artists_id` ASC) VISIBLE,
  CONSTRAINT `fk_tracks_albums1`
    FOREIGN KEY (`albums_id`)
    REFERENCES `tune_buddy`.`albums` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tracks_artists1`
    FOREIGN KEY (`artists_id`)
    REFERENCES `tune_buddy`.`artists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tune_buddy`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tune_buddy`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(20) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `ban_expiration_date` DATE NULL DEFAULT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `roles_id` INT NOT NULL,
  `avatar_url` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  INDEX `fk_users_roles1_idx` (`roles_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_roles1`
    FOREIGN KEY (`roles_id`)
    REFERENCES `tune_buddy`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tune_buddy`.`tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`tokens` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tune_buddy`.`playlists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`playlists` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `users_id` INT NOT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `cover_url` VARCHAR(255) NOT NULL,
  `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`),
  INDEX `fk_playlists_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `tune_buddy`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tune_buddy`.`playlists_tracks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tune_buddy`.`playlists_tracks` (
  `tracks_id` INT NOT NULL,
  `playlists_id` INT NOT NULL,
  PRIMARY KEY (`tracks_id`, `playlists_id`),
  INDEX `fk_playlists_tracks_tracks_idx` (`tracks_id` ASC) VISIBLE,
  INDEX `fk_playlists_tracks_playlists1_idx` (`playlists_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_tracks_tracks`
    FOREIGN KEY (`tracks_id`)
    REFERENCES `tune_buddy`.`tracks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlists_tracks_playlists1`
    FOREIGN KEY (`playlists_id`)
    REFERENCES `tune_buddy`.`playlists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
